<?php
    require_once '/common.php';
    
    use data_models\ShipmentQuery as ShipmentQuery;
    use data_models\ClientQuery as ClientQuery;
    use data_models\BankQuery as BankQuery;
    use data_models\CompanyQuery as CompanyQuery;
    use data_models\ProductQuery as ProductQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0){
        $bankId = isset($data->bankId) ? $data->bankId : 0;

        $bank = BankQuery::create()->findPk($bankId);
        $client = ClientQuery::create()->findPk($bank->getClientId());
        $data = array(
                "id" => $bank->getId(),
                "bankName" => $bank->getBankName(),
                "clientId" => $bank->getClientId(),
                "companyId" => $client->getCompanyId(),
                "branchCode" => $bank->getBranchCode(),
                "accountNumber" => $bank->getAccountNumber(),
                "currency" => $bank->getCurrency(),
                "accountName" => $bank->getAccountName(),
                "city" => $bank->getCity(),
                "swiftCode" => $bank->getSwiftCode(),
                "bankAddress" => $bank->getBankAddress(),
                "clientName" => $client->getClientName(),
                "telNo" => $bank->getTelNo(),
                "fax" => $bank->getFax(),
                "website" => $bank->getWebsite(),
                "email" => $bank->getEmail()
            );
            
        sendSuccessResponse($data, "Bank details retrieved successfully.");
    }
?>