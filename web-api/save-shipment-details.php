<?php
    require_once '/common.php';
    
    use data_models\Shipment as Shipment;
    use data_models\Product as Product;
    use data_models\ShipmentQuery as ShipmentQuery;
    use data_models\ProductQuery as ProductQuery;
    
    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);
    if($data && $data !== null) {
        $shipmentId = isset($data->id) ? $data->id : 0;
        $supplierDateLoad = isset($data->supplierDateLoad) ? $data->supplierDateLoad : '';
        $boatDateLoad = isset($data->boatDateLoad) ? $data->boatDateLoad : '';
        $loadPort = isset($data->loadPort) ? $data->loadPort : '';
        $destinationPort = isset($data->destinationPort) ? $data->destinationPort : '';
        $weekNo = isset($data->weekNo) ? $data->weekNo : '';
        $noOfContainer = isset($data->noOfContainer) ? $data->noOfContainer : '';
        $exporter = isset($data->exporter) ? $data->exporter : '';
        $importer = isset($data->importer) ? $data->importer : '';
        $clientId = isset($data->clientId) ? $data->clientId : '';
        $bankId = isset($data->bankId) ? $data->bankId : '';
        $note = isset($data->note) ? $data->note : '';
        $cashOption = isset($data->cashOption) ? $data->cashOption : '';
        $invoiceMaxPrice = isset($data->invoiceMaxPrice) ? $data->invoiceMaxPrice : '';
        $notes = isset($data->notes) ? $data->notes : '';
        $bankContact = isset($data->bankContact) ? $data->bankContact : '';
        $insured = isset($data->insured) ? $data->insured : '';
        $extraBruto = isset($data->extraBruto) ? $data->extraBruto : '';
        $shippingCost = isset($data->shippingCost) ? $data->shippingCost : '';
        $products = isset($data->products) ? $data->products : array();
        $currency = isset($data->currency) ? $data->currency : '';
        $pricePer = isset($data->pricePer) ? $data->pricePer : '';
        $customName = isset($data->customName) ? $data->customName : '';
        $genAdd = isset($data->genAdd) ? $data->genAdd : '';
        $shipmentAgend = isset($data->shipmentAgend) ? $data->shipmentAgend : '';
    } else {
        $shipmentId = $_POST['id'];
        $supplierDateLoad = $_POST['supplierDateLoad'];
        $boatDateLoad = $_POST['boatDateLoad'];
        $loadPort = $_POST['loadPort'];
        $destinationPort = $_POST['destinationPort'];
        $weekNo = $_POST['weekNo'];
        $noOfContainer = $_POST['noOfContainer'];
        $exporter = $_POST['exporter'];
        $importer = $_POST['importer'];
        $clientId = $_POST['clientId'];
        $bankId = $_POST['bankId'];
        $note = $_POST['note'];
        $cashOption = $_POST['cashOption'];
        $invoiceMaxPrice = $_POST['invoiceMaxPrice'];
        $notes = $_POST['notes'];
        $bankContact = $_POST['bankContact'];
        $insured = $_POST['insured'];
        $extraBruto = $_POST['extraBruto'];
        $shippingCost = $_POST['shippingCost'];
        $products = $_POST['products'];
        $currency = $_POST['currency'];
        $pricePer = $_POST['pricePer'];
        $customName = $_POST['customName'];
        $genAdd = $_POST['genAdd'];
        $shipmentAgend = $_POST['shipmentAgend'];
    }

    if($shipmentId && $shipmentId > 0) {
        $shipment = ShipmentQuery::create()->findOneById($shipmentId);

        ProductQuery::create()->filterByShipmentId($shipmentId)->delete();
    } else {
        $shipment = new Shipment();
    }

    $shipment->setSupplierDateLoad($supplierDateLoad);
    $shipment->setBoatDateLoad($boatDateLoad);
    $shipment->setLoadPort($loadPort);
    $shipment->setDestinationPort($destinationPort);
    $shipment->setWeekNo($weekNo);
    $shipment->setNoOfContainer($noOfContainer);
    $shipment->setExporter($exporter);
    $shipment->setImporter($importer);
    $shipment->setBankId($bankId);
    $shipment->setNote($note);
    $shipment->setBankContact($bankContact);
    $shipment->setInsured($insured);
    $shipment->setExtraBruto($extraBruto);
    $shipment->setShippingCost($shippingCost);
    $shipment->setCashOption($cashOption);
    $shipment->setInvoiceMaxPrice($invoiceMaxPrice);
    $shipment->setNotes($notes);
    $shipment->setCurrency($currency);
    $shipment->setPricePer($pricePer);
    $shipment->setCustomName($customName);
    $shipment->setGenAdd($genAdd);
    $shipment->setShipmentAgend($shipmentAgend);
    $shipment->save();

    foreach($products as $productDetails) {
        $product = new Product();
        $product->setProductName($productDetails->productName);
        $product->setDescription($productDetails->description);
        $product->setBuyPrice($productDetails->buyPrice);
        $product->setSellPrice($productDetails->sellPrice);
        $product->setShipmentId($shipment->getId());
        $product->save();
    }

    sendSuccessResponse($data, "Shipment details is saved successfully.");
?>