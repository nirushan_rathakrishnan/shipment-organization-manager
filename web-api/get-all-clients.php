<?php
    require_once '/common.php';
    
    use data_models\ClientQuery as ClientQuery;
    use data_models\CompanyQuery as CompanyQuery;
    
    $clients = ClientQuery::create()->find();
    
    $data = array();
    foreach($clients as $client) {
      array_push($data, array(
          "id" => $client->getId(), 
          "companyName" => CompanyQuery::create()->findPK($client->getCompanyId())->getCompanyName(),
          "clientName" => $client->getClientName(), 
          "telNo" => $client->getTelNo(), 
          "email" => $client->getEmail()));
    }

    sendSuccessResponse($data, "Clients details retrieved successfully.");
?>