<?php
    require_once '/common.php';
    
    use data_models\CompanyQuery as CompanyQuery;

    $companies = CompanyQuery::create()->find();
    
    $data = array();
    foreach($companies as $company) {
      array_push($data, array(
          "id" => $company->getId(), 
          "companyName" => $company->getCompanyName(), 
          "telNo" => $company->getTelNo(), 
          "email" => $company->getEmail()));
    }

    sendSuccessResponse($data, "Companies details retrieved successfully.");
?>