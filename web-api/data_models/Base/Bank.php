<?php

namespace data_models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use data_models\Bank as ChildBank;
use data_models\BankQuery as ChildBankQuery;
use data_models\Client as ChildClient;
use data_models\ClientQuery as ChildClientQuery;
use data_models\Shipment as ChildShipment;
use data_models\ShipmentQuery as ChildShipmentQuery;
use data_models\Map\BankTableMap;
use data_models\Map\ShipmentTableMap;

/**
 * Base class that represents a row from the 'bank' table.
 *
 *
 *
 * @package    propel.generator.data_models.Base
 */
abstract class Bank implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\data_models\\Map\\BankTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the bankname field.
     *
     * @var        string
     */
    protected $bankname;

    /**
     * The value for the branchcode field.
     *
     * @var        string
     */
    protected $branchcode;

    /**
     * The value for the accountnumber field.
     *
     * @var        string
     */
    protected $accountnumber;

    /**
     * The value for the city field.
     *
     * @var        string
     */
    protected $city;

    /**
     * The value for the swiftcode field.
     *
     * @var        string
     */
    protected $swiftcode;

    /**
     * The value for the bankaddress field.
     *
     * @var        string
     */
    protected $bankaddress;

    /**
     * The value for the telno field.
     *
     * @var        string
     */
    protected $telno;

    /**
     * The value for the fax field.
     *
     * @var        string
     */
    protected $fax;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the website field.
     *
     * @var        string
     */
    protected $website;

    /**
     * The value for the accountname field.
     *
     * @var        string
     */
    protected $accountname;

    /**
     * The value for the currency field.
     *
     * @var        string
     */
    protected $currency;

    /**
     * The value for the client_id field.
     *
     * @var        int
     */
    protected $client_id;

    /**
     * @var        ChildClient
     */
    protected $aClient;

    /**
     * @var        ObjectCollection|ChildShipment[] Collection to store aggregation of ChildShipment objects.
     */
    protected $collShipments;
    protected $collShipmentsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildShipment[]
     */
    protected $shipmentsScheduledForDeletion = null;

    /**
     * Initializes internal state of data_models\Base\Bank object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Bank</code> instance.  If
     * <code>obj</code> is an instance of <code>Bank</code>, delegates to
     * <code>equals(Bank)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Bank The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [bankname] column value.
     *
     * @return string
     */
    public function getBankname()
    {
        return $this->bankname;
    }

    /**
     * Get the [branchcode] column value.
     *
     * @return string
     */
    public function getBranchcode()
    {
        return $this->branchcode;
    }

    /**
     * Get the [accountnumber] column value.
     *
     * @return string
     */
    public function getAccountnumber()
    {
        return $this->accountnumber;
    }

    /**
     * Get the [city] column value.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get the [swiftcode] column value.
     *
     * @return string
     */
    public function getSwiftcode()
    {
        return $this->swiftcode;
    }

    /**
     * Get the [bankaddress] column value.
     *
     * @return string
     */
    public function getBankaddress()
    {
        return $this->bankaddress;
    }

    /**
     * Get the [telno] column value.
     *
     * @return string
     */
    public function getTelno()
    {
        return $this->telno;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [website] column value.
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Get the [accountname] column value.
     *
     * @return string
     */
    public function getAccountname()
    {
        return $this->accountname;
    }

    /**
     * Get the [currency] column value.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Get the [client_id] column value.
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[BankTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [bankname] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setBankname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bankname !== $v) {
            $this->bankname = $v;
            $this->modifiedColumns[BankTableMap::COL_BANKNAME] = true;
        }

        return $this;
    } // setBankname()

    /**
     * Set the value of [branchcode] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setBranchcode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->branchcode !== $v) {
            $this->branchcode = $v;
            $this->modifiedColumns[BankTableMap::COL_BRANCHCODE] = true;
        }

        return $this;
    } // setBranchcode()

    /**
     * Set the value of [accountnumber] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setAccountnumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->accountnumber !== $v) {
            $this->accountnumber = $v;
            $this->modifiedColumns[BankTableMap::COL_ACCOUNTNUMBER] = true;
        }

        return $this;
    } // setAccountnumber()

    /**
     * Set the value of [city] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city !== $v) {
            $this->city = $v;
            $this->modifiedColumns[BankTableMap::COL_CITY] = true;
        }

        return $this;
    } // setCity()

    /**
     * Set the value of [swiftcode] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setSwiftcode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->swiftcode !== $v) {
            $this->swiftcode = $v;
            $this->modifiedColumns[BankTableMap::COL_SWIFTCODE] = true;
        }

        return $this;
    } // setSwiftcode()

    /**
     * Set the value of [bankaddress] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setBankaddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bankaddress !== $v) {
            $this->bankaddress = $v;
            $this->modifiedColumns[BankTableMap::COL_BANKADDRESS] = true;
        }

        return $this;
    } // setBankaddress()

    /**
     * Set the value of [telno] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setTelno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telno !== $v) {
            $this->telno = $v;
            $this->modifiedColumns[BankTableMap::COL_TELNO] = true;
        }

        return $this;
    } // setTelno()

    /**
     * Set the value of [fax] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[BankTableMap::COL_FAX] = true;
        }

        return $this;
    } // setFax()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[BankTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [website] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setWebsite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->website !== $v) {
            $this->website = $v;
            $this->modifiedColumns[BankTableMap::COL_WEBSITE] = true;
        }

        return $this;
    } // setWebsite()

    /**
     * Set the value of [accountname] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setAccountname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->accountname !== $v) {
            $this->accountname = $v;
            $this->modifiedColumns[BankTableMap::COL_ACCOUNTNAME] = true;
        }

        return $this;
    } // setAccountname()

    /**
     * Set the value of [currency] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setCurrency($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->currency !== $v) {
            $this->currency = $v;
            $this->modifiedColumns[BankTableMap::COL_CURRENCY] = true;
        }

        return $this;
    } // setCurrency()

    /**
     * Set the value of [client_id] column.
     *
     * @param int $v new value
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function setClientId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->client_id !== $v) {
            $this->client_id = $v;
            $this->modifiedColumns[BankTableMap::COL_CLIENT_ID] = true;
        }

        if ($this->aClient !== null && $this->aClient->getId() !== $v) {
            $this->aClient = null;
        }

        return $this;
    } // setClientId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BankTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BankTableMap::translateFieldName('Bankname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bankname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BankTableMap::translateFieldName('Branchcode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->branchcode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BankTableMap::translateFieldName('Accountnumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->accountnumber = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BankTableMap::translateFieldName('City', TableMap::TYPE_PHPNAME, $indexType)];
            $this->city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BankTableMap::translateFieldName('Swiftcode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->swiftcode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : BankTableMap::translateFieldName('Bankaddress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bankaddress = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : BankTableMap::translateFieldName('Telno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telno = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : BankTableMap::translateFieldName('Fax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : BankTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : BankTableMap::translateFieldName('Website', TableMap::TYPE_PHPNAME, $indexType)];
            $this->website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : BankTableMap::translateFieldName('Accountname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->accountname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : BankTableMap::translateFieldName('Currency', TableMap::TYPE_PHPNAME, $indexType)];
            $this->currency = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : BankTableMap::translateFieldName('ClientId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->client_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = BankTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\data_models\\Bank'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aClient !== null && $this->client_id !== $this->aClient->getId()) {
            $this->aClient = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BankTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBankQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aClient = null;
            $this->collShipments = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Bank::setDeleted()
     * @see Bank::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBankQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BankTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aClient !== null) {
                if ($this->aClient->isModified() || $this->aClient->isNew()) {
                    $affectedRows += $this->aClient->save($con);
                }
                $this->setClient($this->aClient);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->shipmentsScheduledForDeletion !== null) {
                if (!$this->shipmentsScheduledForDeletion->isEmpty()) {
                    \data_models\ShipmentQuery::create()
                        ->filterByPrimaryKeys($this->shipmentsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->shipmentsScheduledForDeletion = null;
                }
            }

            if ($this->collShipments !== null) {
                foreach ($this->collShipments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BankTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BankTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BankTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(BankTableMap::COL_BANKNAME)) {
            $modifiedColumns[':p' . $index++]  = 'bankName';
        }
        if ($this->isColumnModified(BankTableMap::COL_BRANCHCODE)) {
            $modifiedColumns[':p' . $index++]  = 'branchCode';
        }
        if ($this->isColumnModified(BankTableMap::COL_ACCOUNTNUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'accountNumber';
        }
        if ($this->isColumnModified(BankTableMap::COL_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'city';
        }
        if ($this->isColumnModified(BankTableMap::COL_SWIFTCODE)) {
            $modifiedColumns[':p' . $index++]  = 'swiftCode';
        }
        if ($this->isColumnModified(BankTableMap::COL_BANKADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'bankAddress';
        }
        if ($this->isColumnModified(BankTableMap::COL_TELNO)) {
            $modifiedColumns[':p' . $index++]  = 'telNo';
        }
        if ($this->isColumnModified(BankTableMap::COL_FAX)) {
            $modifiedColumns[':p' . $index++]  = 'fax';
        }
        if ($this->isColumnModified(BankTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(BankTableMap::COL_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'website';
        }
        if ($this->isColumnModified(BankTableMap::COL_ACCOUNTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'accountName';
        }
        if ($this->isColumnModified(BankTableMap::COL_CURRENCY)) {
            $modifiedColumns[':p' . $index++]  = 'currency';
        }
        if ($this->isColumnModified(BankTableMap::COL_CLIENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'client_id';
        }

        $sql = sprintf(
            'INSERT INTO bank (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'bankName':
                        $stmt->bindValue($identifier, $this->bankname, PDO::PARAM_STR);
                        break;
                    case 'branchCode':
                        $stmt->bindValue($identifier, $this->branchcode, PDO::PARAM_STR);
                        break;
                    case 'accountNumber':
                        $stmt->bindValue($identifier, $this->accountnumber, PDO::PARAM_STR);
                        break;
                    case 'city':
                        $stmt->bindValue($identifier, $this->city, PDO::PARAM_STR);
                        break;
                    case 'swiftCode':
                        $stmt->bindValue($identifier, $this->swiftcode, PDO::PARAM_STR);
                        break;
                    case 'bankAddress':
                        $stmt->bindValue($identifier, $this->bankaddress, PDO::PARAM_STR);
                        break;
                    case 'telNo':
                        $stmt->bindValue($identifier, $this->telno, PDO::PARAM_STR);
                        break;
                    case 'fax':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'website':
                        $stmt->bindValue($identifier, $this->website, PDO::PARAM_STR);
                        break;
                    case 'accountName':
                        $stmt->bindValue($identifier, $this->accountname, PDO::PARAM_STR);
                        break;
                    case 'currency':
                        $stmt->bindValue($identifier, $this->currency, PDO::PARAM_STR);
                        break;
                    case 'client_id':
                        $stmt->bindValue($identifier, $this->client_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BankTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBankname();
                break;
            case 2:
                return $this->getBranchcode();
                break;
            case 3:
                return $this->getAccountnumber();
                break;
            case 4:
                return $this->getCity();
                break;
            case 5:
                return $this->getSwiftcode();
                break;
            case 6:
                return $this->getBankaddress();
                break;
            case 7:
                return $this->getTelno();
                break;
            case 8:
                return $this->getFax();
                break;
            case 9:
                return $this->getEmail();
                break;
            case 10:
                return $this->getWebsite();
                break;
            case 11:
                return $this->getAccountname();
                break;
            case 12:
                return $this->getCurrency();
                break;
            case 13:
                return $this->getClientId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Bank'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Bank'][$this->hashCode()] = true;
        $keys = BankTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBankname(),
            $keys[2] => $this->getBranchcode(),
            $keys[3] => $this->getAccountnumber(),
            $keys[4] => $this->getCity(),
            $keys[5] => $this->getSwiftcode(),
            $keys[6] => $this->getBankaddress(),
            $keys[7] => $this->getTelno(),
            $keys[8] => $this->getFax(),
            $keys[9] => $this->getEmail(),
            $keys[10] => $this->getWebsite(),
            $keys[11] => $this->getAccountname(),
            $keys[12] => $this->getCurrency(),
            $keys[13] => $this->getClientId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aClient) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'client';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'client';
                        break;
                    default:
                        $key = 'Client';
                }

                $result[$key] = $this->aClient->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collShipments) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'shipments';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'shipments';
                        break;
                    default:
                        $key = 'Shipments';
                }

                $result[$key] = $this->collShipments->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\data_models\Bank
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BankTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\data_models\Bank
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBankname($value);
                break;
            case 2:
                $this->setBranchcode($value);
                break;
            case 3:
                $this->setAccountnumber($value);
                break;
            case 4:
                $this->setCity($value);
                break;
            case 5:
                $this->setSwiftcode($value);
                break;
            case 6:
                $this->setBankaddress($value);
                break;
            case 7:
                $this->setTelno($value);
                break;
            case 8:
                $this->setFax($value);
                break;
            case 9:
                $this->setEmail($value);
                break;
            case 10:
                $this->setWebsite($value);
                break;
            case 11:
                $this->setAccountname($value);
                break;
            case 12:
                $this->setCurrency($value);
                break;
            case 13:
                $this->setClientId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BankTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBankname($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBranchcode($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAccountnumber($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCity($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSwiftcode($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setBankaddress($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTelno($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setFax($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setEmail($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setWebsite($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setAccountname($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCurrency($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setClientId($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\data_models\Bank The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BankTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BankTableMap::COL_ID)) {
            $criteria->add(BankTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(BankTableMap::COL_BANKNAME)) {
            $criteria->add(BankTableMap::COL_BANKNAME, $this->bankname);
        }
        if ($this->isColumnModified(BankTableMap::COL_BRANCHCODE)) {
            $criteria->add(BankTableMap::COL_BRANCHCODE, $this->branchcode);
        }
        if ($this->isColumnModified(BankTableMap::COL_ACCOUNTNUMBER)) {
            $criteria->add(BankTableMap::COL_ACCOUNTNUMBER, $this->accountnumber);
        }
        if ($this->isColumnModified(BankTableMap::COL_CITY)) {
            $criteria->add(BankTableMap::COL_CITY, $this->city);
        }
        if ($this->isColumnModified(BankTableMap::COL_SWIFTCODE)) {
            $criteria->add(BankTableMap::COL_SWIFTCODE, $this->swiftcode);
        }
        if ($this->isColumnModified(BankTableMap::COL_BANKADDRESS)) {
            $criteria->add(BankTableMap::COL_BANKADDRESS, $this->bankaddress);
        }
        if ($this->isColumnModified(BankTableMap::COL_TELNO)) {
            $criteria->add(BankTableMap::COL_TELNO, $this->telno);
        }
        if ($this->isColumnModified(BankTableMap::COL_FAX)) {
            $criteria->add(BankTableMap::COL_FAX, $this->fax);
        }
        if ($this->isColumnModified(BankTableMap::COL_EMAIL)) {
            $criteria->add(BankTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(BankTableMap::COL_WEBSITE)) {
            $criteria->add(BankTableMap::COL_WEBSITE, $this->website);
        }
        if ($this->isColumnModified(BankTableMap::COL_ACCOUNTNAME)) {
            $criteria->add(BankTableMap::COL_ACCOUNTNAME, $this->accountname);
        }
        if ($this->isColumnModified(BankTableMap::COL_CURRENCY)) {
            $criteria->add(BankTableMap::COL_CURRENCY, $this->currency);
        }
        if ($this->isColumnModified(BankTableMap::COL_CLIENT_ID)) {
            $criteria->add(BankTableMap::COL_CLIENT_ID, $this->client_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBankQuery::create();
        $criteria->add(BankTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \data_models\Bank (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBankname($this->getBankname());
        $copyObj->setBranchcode($this->getBranchcode());
        $copyObj->setAccountnumber($this->getAccountnumber());
        $copyObj->setCity($this->getCity());
        $copyObj->setSwiftcode($this->getSwiftcode());
        $copyObj->setBankaddress($this->getBankaddress());
        $copyObj->setTelno($this->getTelno());
        $copyObj->setFax($this->getFax());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setWebsite($this->getWebsite());
        $copyObj->setAccountname($this->getAccountname());
        $copyObj->setCurrency($this->getCurrency());
        $copyObj->setClientId($this->getClientId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getShipments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addShipment($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \data_models\Bank Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildClient object.
     *
     * @param  ChildClient $v
     * @return $this|\data_models\Bank The current object (for fluent API support)
     * @throws PropelException
     */
    public function setClient(ChildClient $v = null)
    {
        if ($v === null) {
            $this->setClientId(NULL);
        } else {
            $this->setClientId($v->getId());
        }

        $this->aClient = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildClient object, it will not be re-added.
        if ($v !== null) {
            $v->addBank($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildClient object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildClient The associated ChildClient object.
     * @throws PropelException
     */
    public function getClient(ConnectionInterface $con = null)
    {
        if ($this->aClient === null && ($this->client_id != 0)) {
            $this->aClient = ChildClientQuery::create()->findPk($this->client_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aClient->addBanks($this);
             */
        }

        return $this->aClient;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Shipment' == $relationName) {
            $this->initShipments();
            return;
        }
    }

    /**
     * Clears out the collShipments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addShipments()
     */
    public function clearShipments()
    {
        $this->collShipments = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collShipments collection loaded partially.
     */
    public function resetPartialShipments($v = true)
    {
        $this->collShipmentsPartial = $v;
    }

    /**
     * Initializes the collShipments collection.
     *
     * By default this just sets the collShipments collection to an empty array (like clearcollShipments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initShipments($overrideExisting = true)
    {
        if (null !== $this->collShipments && !$overrideExisting) {
            return;
        }

        $collectionClassName = ShipmentTableMap::getTableMap()->getCollectionClassName();

        $this->collShipments = new $collectionClassName;
        $this->collShipments->setModel('\data_models\Shipment');
    }

    /**
     * Gets an array of ChildShipment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBank is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildShipment[] List of ChildShipment objects
     * @throws PropelException
     */
    public function getShipments(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collShipmentsPartial && !$this->isNew();
        if (null === $this->collShipments || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collShipments) {
                // return empty collection
                $this->initShipments();
            } else {
                $collShipments = ChildShipmentQuery::create(null, $criteria)
                    ->filterByBank($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collShipmentsPartial && count($collShipments)) {
                        $this->initShipments(false);

                        foreach ($collShipments as $obj) {
                            if (false == $this->collShipments->contains($obj)) {
                                $this->collShipments->append($obj);
                            }
                        }

                        $this->collShipmentsPartial = true;
                    }

                    return $collShipments;
                }

                if ($partial && $this->collShipments) {
                    foreach ($this->collShipments as $obj) {
                        if ($obj->isNew()) {
                            $collShipments[] = $obj;
                        }
                    }
                }

                $this->collShipments = $collShipments;
                $this->collShipmentsPartial = false;
            }
        }

        return $this->collShipments;
    }

    /**
     * Sets a collection of ChildShipment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $shipments A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBank The current object (for fluent API support)
     */
    public function setShipments(Collection $shipments, ConnectionInterface $con = null)
    {
        /** @var ChildShipment[] $shipmentsToDelete */
        $shipmentsToDelete = $this->getShipments(new Criteria(), $con)->diff($shipments);


        $this->shipmentsScheduledForDeletion = $shipmentsToDelete;

        foreach ($shipmentsToDelete as $shipmentRemoved) {
            $shipmentRemoved->setBank(null);
        }

        $this->collShipments = null;
        foreach ($shipments as $shipment) {
            $this->addShipment($shipment);
        }

        $this->collShipments = $shipments;
        $this->collShipmentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Shipment objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Shipment objects.
     * @throws PropelException
     */
    public function countShipments(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collShipmentsPartial && !$this->isNew();
        if (null === $this->collShipments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collShipments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getShipments());
            }

            $query = ChildShipmentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBank($this)
                ->count($con);
        }

        return count($this->collShipments);
    }

    /**
     * Method called to associate a ChildShipment object to this object
     * through the ChildShipment foreign key attribute.
     *
     * @param  ChildShipment $l ChildShipment
     * @return $this|\data_models\Bank The current object (for fluent API support)
     */
    public function addShipment(ChildShipment $l)
    {
        if ($this->collShipments === null) {
            $this->initShipments();
            $this->collShipmentsPartial = true;
        }

        if (!$this->collShipments->contains($l)) {
            $this->doAddShipment($l);

            if ($this->shipmentsScheduledForDeletion and $this->shipmentsScheduledForDeletion->contains($l)) {
                $this->shipmentsScheduledForDeletion->remove($this->shipmentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildShipment $shipment The ChildShipment object to add.
     */
    protected function doAddShipment(ChildShipment $shipment)
    {
        $this->collShipments[]= $shipment;
        $shipment->setBank($this);
    }

    /**
     * @param  ChildShipment $shipment The ChildShipment object to remove.
     * @return $this|ChildBank The current object (for fluent API support)
     */
    public function removeShipment(ChildShipment $shipment)
    {
        if ($this->getShipments()->contains($shipment)) {
            $pos = $this->collShipments->search($shipment);
            $this->collShipments->remove($pos);
            if (null === $this->shipmentsScheduledForDeletion) {
                $this->shipmentsScheduledForDeletion = clone $this->collShipments;
                $this->shipmentsScheduledForDeletion->clear();
            }
            $this->shipmentsScheduledForDeletion[]= clone $shipment;
            $shipment->setBank(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aClient) {
            $this->aClient->removeBank($this);
        }
        $this->id = null;
        $this->bankname = null;
        $this->branchcode = null;
        $this->accountnumber = null;
        $this->city = null;
        $this->swiftcode = null;
        $this->bankaddress = null;
        $this->telno = null;
        $this->fax = null;
        $this->email = null;
        $this->website = null;
        $this->accountname = null;
        $this->currency = null;
        $this->client_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collShipments) {
                foreach ($this->collShipments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collShipments = null;
        $this->aClient = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BankTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
