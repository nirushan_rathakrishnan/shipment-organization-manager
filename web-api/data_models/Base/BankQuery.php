<?php

namespace data_models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use data_models\Bank as ChildBank;
use data_models\BankQuery as ChildBankQuery;
use data_models\Map\BankTableMap;

/**
 * Base class that represents a query for the 'bank' table.
 *
 *
 *
 * @method     ChildBankQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBankQuery orderByBankname($order = Criteria::ASC) Order by the bankName column
 * @method     ChildBankQuery orderByBranchcode($order = Criteria::ASC) Order by the branchCode column
 * @method     ChildBankQuery orderByAccountnumber($order = Criteria::ASC) Order by the accountNumber column
 * @method     ChildBankQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     ChildBankQuery orderBySwiftcode($order = Criteria::ASC) Order by the swiftCode column
 * @method     ChildBankQuery orderByBankaddress($order = Criteria::ASC) Order by the bankAddress column
 * @method     ChildBankQuery orderByTelno($order = Criteria::ASC) Order by the telNo column
 * @method     ChildBankQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method     ChildBankQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildBankQuery orderByWebsite($order = Criteria::ASC) Order by the website column
 * @method     ChildBankQuery orderByAccountname($order = Criteria::ASC) Order by the accountName column
 * @method     ChildBankQuery orderByCurrency($order = Criteria::ASC) Order by the currency column
 * @method     ChildBankQuery orderByClientId($order = Criteria::ASC) Order by the client_id column
 *
 * @method     ChildBankQuery groupById() Group by the id column
 * @method     ChildBankQuery groupByBankname() Group by the bankName column
 * @method     ChildBankQuery groupByBranchcode() Group by the branchCode column
 * @method     ChildBankQuery groupByAccountnumber() Group by the accountNumber column
 * @method     ChildBankQuery groupByCity() Group by the city column
 * @method     ChildBankQuery groupBySwiftcode() Group by the swiftCode column
 * @method     ChildBankQuery groupByBankaddress() Group by the bankAddress column
 * @method     ChildBankQuery groupByTelno() Group by the telNo column
 * @method     ChildBankQuery groupByFax() Group by the fax column
 * @method     ChildBankQuery groupByEmail() Group by the email column
 * @method     ChildBankQuery groupByWebsite() Group by the website column
 * @method     ChildBankQuery groupByAccountname() Group by the accountName column
 * @method     ChildBankQuery groupByCurrency() Group by the currency column
 * @method     ChildBankQuery groupByClientId() Group by the client_id column
 *
 * @method     ChildBankQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBankQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBankQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBankQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBankQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBankQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBankQuery leftJoinClient($relationAlias = null) Adds a LEFT JOIN clause to the query using the Client relation
 * @method     ChildBankQuery rightJoinClient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Client relation
 * @method     ChildBankQuery innerJoinClient($relationAlias = null) Adds a INNER JOIN clause to the query using the Client relation
 *
 * @method     ChildBankQuery joinWithClient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Client relation
 *
 * @method     ChildBankQuery leftJoinWithClient() Adds a LEFT JOIN clause and with to the query using the Client relation
 * @method     ChildBankQuery rightJoinWithClient() Adds a RIGHT JOIN clause and with to the query using the Client relation
 * @method     ChildBankQuery innerJoinWithClient() Adds a INNER JOIN clause and with to the query using the Client relation
 *
 * @method     ChildBankQuery leftJoinShipment($relationAlias = null) Adds a LEFT JOIN clause to the query using the Shipment relation
 * @method     ChildBankQuery rightJoinShipment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Shipment relation
 * @method     ChildBankQuery innerJoinShipment($relationAlias = null) Adds a INNER JOIN clause to the query using the Shipment relation
 *
 * @method     ChildBankQuery joinWithShipment($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Shipment relation
 *
 * @method     ChildBankQuery leftJoinWithShipment() Adds a LEFT JOIN clause and with to the query using the Shipment relation
 * @method     ChildBankQuery rightJoinWithShipment() Adds a RIGHT JOIN clause and with to the query using the Shipment relation
 * @method     ChildBankQuery innerJoinWithShipment() Adds a INNER JOIN clause and with to the query using the Shipment relation
 *
 * @method     \data_models\ClientQuery|\data_models\ShipmentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBank findOne(ConnectionInterface $con = null) Return the first ChildBank matching the query
 * @method     ChildBank findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBank matching the query, or a new ChildBank object populated from the query conditions when no match is found
 *
 * @method     ChildBank findOneById(int $id) Return the first ChildBank filtered by the id column
 * @method     ChildBank findOneByBankname(string $bankName) Return the first ChildBank filtered by the bankName column
 * @method     ChildBank findOneByBranchcode(string $branchCode) Return the first ChildBank filtered by the branchCode column
 * @method     ChildBank findOneByAccountnumber(string $accountNumber) Return the first ChildBank filtered by the accountNumber column
 * @method     ChildBank findOneByCity(string $city) Return the first ChildBank filtered by the city column
 * @method     ChildBank findOneBySwiftcode(string $swiftCode) Return the first ChildBank filtered by the swiftCode column
 * @method     ChildBank findOneByBankaddress(string $bankAddress) Return the first ChildBank filtered by the bankAddress column
 * @method     ChildBank findOneByTelno(string $telNo) Return the first ChildBank filtered by the telNo column
 * @method     ChildBank findOneByFax(string $fax) Return the first ChildBank filtered by the fax column
 * @method     ChildBank findOneByEmail(string $email) Return the first ChildBank filtered by the email column
 * @method     ChildBank findOneByWebsite(string $website) Return the first ChildBank filtered by the website column
 * @method     ChildBank findOneByAccountname(string $accountName) Return the first ChildBank filtered by the accountName column
 * @method     ChildBank findOneByCurrency(string $currency) Return the first ChildBank filtered by the currency column
 * @method     ChildBank findOneByClientId(int $client_id) Return the first ChildBank filtered by the client_id column *

 * @method     ChildBank requirePk($key, ConnectionInterface $con = null) Return the ChildBank by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOne(ConnectionInterface $con = null) Return the first ChildBank matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBank requireOneById(int $id) Return the first ChildBank filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByBankname(string $bankName) Return the first ChildBank filtered by the bankName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByBranchcode(string $branchCode) Return the first ChildBank filtered by the branchCode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByAccountnumber(string $accountNumber) Return the first ChildBank filtered by the accountNumber column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByCity(string $city) Return the first ChildBank filtered by the city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneBySwiftcode(string $swiftCode) Return the first ChildBank filtered by the swiftCode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByBankaddress(string $bankAddress) Return the first ChildBank filtered by the bankAddress column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByTelno(string $telNo) Return the first ChildBank filtered by the telNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByFax(string $fax) Return the first ChildBank filtered by the fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByEmail(string $email) Return the first ChildBank filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByWebsite(string $website) Return the first ChildBank filtered by the website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByAccountname(string $accountName) Return the first ChildBank filtered by the accountName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByCurrency(string $currency) Return the first ChildBank filtered by the currency column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBank requireOneByClientId(int $client_id) Return the first ChildBank filtered by the client_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBank[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBank objects based on current ModelCriteria
 * @method     ChildBank[]|ObjectCollection findById(int $id) Return ChildBank objects filtered by the id column
 * @method     ChildBank[]|ObjectCollection findByBankname(string $bankName) Return ChildBank objects filtered by the bankName column
 * @method     ChildBank[]|ObjectCollection findByBranchcode(string $branchCode) Return ChildBank objects filtered by the branchCode column
 * @method     ChildBank[]|ObjectCollection findByAccountnumber(string $accountNumber) Return ChildBank objects filtered by the accountNumber column
 * @method     ChildBank[]|ObjectCollection findByCity(string $city) Return ChildBank objects filtered by the city column
 * @method     ChildBank[]|ObjectCollection findBySwiftcode(string $swiftCode) Return ChildBank objects filtered by the swiftCode column
 * @method     ChildBank[]|ObjectCollection findByBankaddress(string $bankAddress) Return ChildBank objects filtered by the bankAddress column
 * @method     ChildBank[]|ObjectCollection findByTelno(string $telNo) Return ChildBank objects filtered by the telNo column
 * @method     ChildBank[]|ObjectCollection findByFax(string $fax) Return ChildBank objects filtered by the fax column
 * @method     ChildBank[]|ObjectCollection findByEmail(string $email) Return ChildBank objects filtered by the email column
 * @method     ChildBank[]|ObjectCollection findByWebsite(string $website) Return ChildBank objects filtered by the website column
 * @method     ChildBank[]|ObjectCollection findByAccountname(string $accountName) Return ChildBank objects filtered by the accountName column
 * @method     ChildBank[]|ObjectCollection findByCurrency(string $currency) Return ChildBank objects filtered by the currency column
 * @method     ChildBank[]|ObjectCollection findByClientId(int $client_id) Return ChildBank objects filtered by the client_id column
 * @method     ChildBank[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BankQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \data_models\Base\BankQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\data_models\\Bank', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBankQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBankQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBankQuery) {
            return $criteria;
        }
        $query = new ChildBankQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBank|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BankTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BankTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBank A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, bankName, branchCode, accountNumber, city, swiftCode, bankAddress, telNo, fax, email, website, accountName, currency, client_id FROM bank WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBank $obj */
            $obj = new ChildBank();
            $obj->hydrate($row);
            BankTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBank|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BankTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BankTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BankTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BankTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the bankName column
     *
     * Example usage:
     * <code>
     * $query->filterByBankname('fooValue');   // WHERE bankName = 'fooValue'
     * $query->filterByBankname('%fooValue%', Criteria::LIKE); // WHERE bankName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bankname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByBankname($bankname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bankname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_BANKNAME, $bankname, $comparison);
    }

    /**
     * Filter the query on the branchCode column
     *
     * Example usage:
     * <code>
     * $query->filterByBranchcode('fooValue');   // WHERE branchCode = 'fooValue'
     * $query->filterByBranchcode('%fooValue%', Criteria::LIKE); // WHERE branchCode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $branchcode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByBranchcode($branchcode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($branchcode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_BRANCHCODE, $branchcode, $comparison);
    }

    /**
     * Filter the query on the accountNumber column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountnumber('fooValue');   // WHERE accountNumber = 'fooValue'
     * $query->filterByAccountnumber('%fooValue%', Criteria::LIKE); // WHERE accountNumber LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountnumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByAccountnumber($accountnumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountnumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_ACCOUNTNUMBER, $accountnumber, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%', Criteria::LIKE); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the swiftCode column
     *
     * Example usage:
     * <code>
     * $query->filterBySwiftcode('fooValue');   // WHERE swiftCode = 'fooValue'
     * $query->filterBySwiftcode('%fooValue%', Criteria::LIKE); // WHERE swiftCode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $swiftcode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterBySwiftcode($swiftcode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($swiftcode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_SWIFTCODE, $swiftcode, $comparison);
    }

    /**
     * Filter the query on the bankAddress column
     *
     * Example usage:
     * <code>
     * $query->filterByBankaddress('fooValue');   // WHERE bankAddress = 'fooValue'
     * $query->filterByBankaddress('%fooValue%', Criteria::LIKE); // WHERE bankAddress LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bankaddress The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByBankaddress($bankaddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bankaddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_BANKADDRESS, $bankaddress, $comparison);
    }

    /**
     * Filter the query on the telNo column
     *
     * Example usage:
     * <code>
     * $query->filterByTelno('fooValue');   // WHERE telNo = 'fooValue'
     * $query->filterByTelno('%fooValue%', Criteria::LIKE); // WHERE telNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telno The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByTelno($telno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telno)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_TELNO, $telno, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%', Criteria::LIKE); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterByWebsite('%fooValue%', Criteria::LIKE); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query on the accountName column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountname('fooValue');   // WHERE accountName = 'fooValue'
     * $query->filterByAccountname('%fooValue%', Criteria::LIKE); // WHERE accountName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByAccountname($accountname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_ACCOUNTNAME, $accountname, $comparison);
    }

    /**
     * Filter the query on the currency column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrency('fooValue');   // WHERE currency = 'fooValue'
     * $query->filterByCurrency('%fooValue%', Criteria::LIKE); // WHERE currency LIKE '%fooValue%'
     * </code>
     *
     * @param     string $currency The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByCurrency($currency = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($currency)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_CURRENCY, $currency, $comparison);
    }

    /**
     * Filter the query on the client_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClientId(1234); // WHERE client_id = 1234
     * $query->filterByClientId(array(12, 34)); // WHERE client_id IN (12, 34)
     * $query->filterByClientId(array('min' => 12)); // WHERE client_id > 12
     * </code>
     *
     * @see       filterByClient()
     *
     * @param     mixed $clientId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function filterByClientId($clientId = null, $comparison = null)
    {
        if (is_array($clientId)) {
            $useMinMax = false;
            if (isset($clientId['min'])) {
                $this->addUsingAlias(BankTableMap::COL_CLIENT_ID, $clientId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clientId['max'])) {
                $this->addUsingAlias(BankTableMap::COL_CLIENT_ID, $clientId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTableMap::COL_CLIENT_ID, $clientId, $comparison);
    }

    /**
     * Filter the query by a related \data_models\Client object
     *
     * @param \data_models\Client|ObjectCollection $client The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBankQuery The current query, for fluid interface
     */
    public function filterByClient($client, $comparison = null)
    {
        if ($client instanceof \data_models\Client) {
            return $this
                ->addUsingAlias(BankTableMap::COL_CLIENT_ID, $client->getId(), $comparison);
        } elseif ($client instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BankTableMap::COL_CLIENT_ID, $client->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByClient() only accepts arguments of type \data_models\Client or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Client relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function joinClient($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Client');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Client');
        }

        return $this;
    }

    /**
     * Use the Client relation Client object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \data_models\ClientQuery A secondary query class using the current class as primary query
     */
    public function useClientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Client', '\data_models\ClientQuery');
    }

    /**
     * Filter the query by a related \data_models\Shipment object
     *
     * @param \data_models\Shipment|ObjectCollection $shipment the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBankQuery The current query, for fluid interface
     */
    public function filterByShipment($shipment, $comparison = null)
    {
        if ($shipment instanceof \data_models\Shipment) {
            return $this
                ->addUsingAlias(BankTableMap::COL_ID, $shipment->getBankId(), $comparison);
        } elseif ($shipment instanceof ObjectCollection) {
            return $this
                ->useShipmentQuery()
                ->filterByPrimaryKeys($shipment->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByShipment() only accepts arguments of type \data_models\Shipment or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Shipment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function joinShipment($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Shipment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Shipment');
        }

        return $this;
    }

    /**
     * Use the Shipment relation Shipment object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \data_models\ShipmentQuery A secondary query class using the current class as primary query
     */
    public function useShipmentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinShipment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Shipment', '\data_models\ShipmentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBank $bank Object to remove from the list of results
     *
     * @return $this|ChildBankQuery The current query, for fluid interface
     */
    public function prune($bank = null)
    {
        if ($bank) {
            $this->addUsingAlias(BankTableMap::COL_ID, $bank->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the bank table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BankTableMap::clearInstancePool();
            BankTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BankTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BankTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BankTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BankQuery
