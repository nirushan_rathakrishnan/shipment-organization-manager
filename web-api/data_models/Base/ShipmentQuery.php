<?php

namespace data_models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use data_models\Shipment as ChildShipment;
use data_models\ShipmentQuery as ChildShipmentQuery;
use data_models\Map\ShipmentTableMap;

/**
 * Base class that represents a query for the 'shipment' table.
 *
 *
 *
 * @method     ChildShipmentQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildShipmentQuery orderBySupplierdateload($order = Criteria::ASC) Order by the supplierDateLoad column
 * @method     ChildShipmentQuery orderByBoatdateload($order = Criteria::ASC) Order by the boatDateLoad column
 * @method     ChildShipmentQuery orderByLoadport($order = Criteria::ASC) Order by the loadPort column
 * @method     ChildShipmentQuery orderByDestinationport($order = Criteria::ASC) Order by the destinationPort column
 * @method     ChildShipmentQuery orderByWeekno($order = Criteria::ASC) Order by the weekNo column
 * @method     ChildShipmentQuery orderByNoofcontainer($order = Criteria::ASC) Order by the noOfContainer column
 * @method     ChildShipmentQuery orderByExporter($order = Criteria::ASC) Order by the exporter column
 * @method     ChildShipmentQuery orderByImporter($order = Criteria::ASC) Order by the importer column
 * @method     ChildShipmentQuery orderByNote($order = Criteria::ASC) Order by the note column
 * @method     ChildShipmentQuery orderByCashoption($order = Criteria::ASC) Order by the cashOption column
 * @method     ChildShipmentQuery orderByInvoicemaxprice($order = Criteria::ASC) Order by the invoiceMaxPrice column
 * @method     ChildShipmentQuery orderByNotes($order = Criteria::ASC) Order by the notes column
 * @method     ChildShipmentQuery orderByBankcontact($order = Criteria::ASC) Order by the bankContact column
 * @method     ChildShipmentQuery orderByInsured($order = Criteria::ASC) Order by the insured column
 * @method     ChildShipmentQuery orderByExtrabruto($order = Criteria::ASC) Order by the extraBruto column
 * @method     ChildShipmentQuery orderByShippingcost($order = Criteria::ASC) Order by the shippingCost column
 * @method     ChildShipmentQuery orderByCurrency($order = Criteria::ASC) Order by the currency column
 * @method     ChildShipmentQuery orderByPriceper($order = Criteria::ASC) Order by the pricePer column
 * @method     ChildShipmentQuery orderByCustomname($order = Criteria::ASC) Order by the customName column
 * @method     ChildShipmentQuery orderByGenadd($order = Criteria::ASC) Order by the genAdd column
 * @method     ChildShipmentQuery orderByShipmentagend($order = Criteria::ASC) Order by the shipmentAgend column
 * @method     ChildShipmentQuery orderByBankId($order = Criteria::ASC) Order by the bank_id column
 *
 * @method     ChildShipmentQuery groupById() Group by the id column
 * @method     ChildShipmentQuery groupBySupplierdateload() Group by the supplierDateLoad column
 * @method     ChildShipmentQuery groupByBoatdateload() Group by the boatDateLoad column
 * @method     ChildShipmentQuery groupByLoadport() Group by the loadPort column
 * @method     ChildShipmentQuery groupByDestinationport() Group by the destinationPort column
 * @method     ChildShipmentQuery groupByWeekno() Group by the weekNo column
 * @method     ChildShipmentQuery groupByNoofcontainer() Group by the noOfContainer column
 * @method     ChildShipmentQuery groupByExporter() Group by the exporter column
 * @method     ChildShipmentQuery groupByImporter() Group by the importer column
 * @method     ChildShipmentQuery groupByNote() Group by the note column
 * @method     ChildShipmentQuery groupByCashoption() Group by the cashOption column
 * @method     ChildShipmentQuery groupByInvoicemaxprice() Group by the invoiceMaxPrice column
 * @method     ChildShipmentQuery groupByNotes() Group by the notes column
 * @method     ChildShipmentQuery groupByBankcontact() Group by the bankContact column
 * @method     ChildShipmentQuery groupByInsured() Group by the insured column
 * @method     ChildShipmentQuery groupByExtrabruto() Group by the extraBruto column
 * @method     ChildShipmentQuery groupByShippingcost() Group by the shippingCost column
 * @method     ChildShipmentQuery groupByCurrency() Group by the currency column
 * @method     ChildShipmentQuery groupByPriceper() Group by the pricePer column
 * @method     ChildShipmentQuery groupByCustomname() Group by the customName column
 * @method     ChildShipmentQuery groupByGenadd() Group by the genAdd column
 * @method     ChildShipmentQuery groupByShipmentagend() Group by the shipmentAgend column
 * @method     ChildShipmentQuery groupByBankId() Group by the bank_id column
 *
 * @method     ChildShipmentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildShipmentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildShipmentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildShipmentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildShipmentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildShipmentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildShipmentQuery leftJoinBank($relationAlias = null) Adds a LEFT JOIN clause to the query using the Bank relation
 * @method     ChildShipmentQuery rightJoinBank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Bank relation
 * @method     ChildShipmentQuery innerJoinBank($relationAlias = null) Adds a INNER JOIN clause to the query using the Bank relation
 *
 * @method     ChildShipmentQuery joinWithBank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Bank relation
 *
 * @method     ChildShipmentQuery leftJoinWithBank() Adds a LEFT JOIN clause and with to the query using the Bank relation
 * @method     ChildShipmentQuery rightJoinWithBank() Adds a RIGHT JOIN clause and with to the query using the Bank relation
 * @method     ChildShipmentQuery innerJoinWithBank() Adds a INNER JOIN clause and with to the query using the Bank relation
 *
 * @method     ChildShipmentQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildShipmentQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildShipmentQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildShipmentQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildShipmentQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildShipmentQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildShipmentQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     \data_models\BankQuery|\data_models\ProductQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildShipment findOne(ConnectionInterface $con = null) Return the first ChildShipment matching the query
 * @method     ChildShipment findOneOrCreate(ConnectionInterface $con = null) Return the first ChildShipment matching the query, or a new ChildShipment object populated from the query conditions when no match is found
 *
 * @method     ChildShipment findOneById(int $id) Return the first ChildShipment filtered by the id column
 * @method     ChildShipment findOneBySupplierdateload(string $supplierDateLoad) Return the first ChildShipment filtered by the supplierDateLoad column
 * @method     ChildShipment findOneByBoatdateload(string $boatDateLoad) Return the first ChildShipment filtered by the boatDateLoad column
 * @method     ChildShipment findOneByLoadport(string $loadPort) Return the first ChildShipment filtered by the loadPort column
 * @method     ChildShipment findOneByDestinationport(string $destinationPort) Return the first ChildShipment filtered by the destinationPort column
 * @method     ChildShipment findOneByWeekno(string $weekNo) Return the first ChildShipment filtered by the weekNo column
 * @method     ChildShipment findOneByNoofcontainer(string $noOfContainer) Return the first ChildShipment filtered by the noOfContainer column
 * @method     ChildShipment findOneByExporter(string $exporter) Return the first ChildShipment filtered by the exporter column
 * @method     ChildShipment findOneByImporter(string $importer) Return the first ChildShipment filtered by the importer column
 * @method     ChildShipment findOneByNote(string $note) Return the first ChildShipment filtered by the note column
 * @method     ChildShipment findOneByCashoption(string $cashOption) Return the first ChildShipment filtered by the cashOption column
 * @method     ChildShipment findOneByInvoicemaxprice(string $invoiceMaxPrice) Return the first ChildShipment filtered by the invoiceMaxPrice column
 * @method     ChildShipment findOneByNotes(string $notes) Return the first ChildShipment filtered by the notes column
 * @method     ChildShipment findOneByBankcontact(string $bankContact) Return the first ChildShipment filtered by the bankContact column
 * @method     ChildShipment findOneByInsured(string $insured) Return the first ChildShipment filtered by the insured column
 * @method     ChildShipment findOneByExtrabruto(string $extraBruto) Return the first ChildShipment filtered by the extraBruto column
 * @method     ChildShipment findOneByShippingcost(string $shippingCost) Return the first ChildShipment filtered by the shippingCost column
 * @method     ChildShipment findOneByCurrency(string $currency) Return the first ChildShipment filtered by the currency column
 * @method     ChildShipment findOneByPriceper(string $pricePer) Return the first ChildShipment filtered by the pricePer column
 * @method     ChildShipment findOneByCustomname(string $customName) Return the first ChildShipment filtered by the customName column
 * @method     ChildShipment findOneByGenadd(string $genAdd) Return the first ChildShipment filtered by the genAdd column
 * @method     ChildShipment findOneByShipmentagend(string $shipmentAgend) Return the first ChildShipment filtered by the shipmentAgend column
 * @method     ChildShipment findOneByBankId(int $bank_id) Return the first ChildShipment filtered by the bank_id column *

 * @method     ChildShipment requirePk($key, ConnectionInterface $con = null) Return the ChildShipment by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOne(ConnectionInterface $con = null) Return the first ChildShipment matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShipment requireOneById(int $id) Return the first ChildShipment filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneBySupplierdateload(string $supplierDateLoad) Return the first ChildShipment filtered by the supplierDateLoad column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByBoatdateload(string $boatDateLoad) Return the first ChildShipment filtered by the boatDateLoad column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByLoadport(string $loadPort) Return the first ChildShipment filtered by the loadPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByDestinationport(string $destinationPort) Return the first ChildShipment filtered by the destinationPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByWeekno(string $weekNo) Return the first ChildShipment filtered by the weekNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByNoofcontainer(string $noOfContainer) Return the first ChildShipment filtered by the noOfContainer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByExporter(string $exporter) Return the first ChildShipment filtered by the exporter column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByImporter(string $importer) Return the first ChildShipment filtered by the importer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByNote(string $note) Return the first ChildShipment filtered by the note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByCashoption(string $cashOption) Return the first ChildShipment filtered by the cashOption column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByInvoicemaxprice(string $invoiceMaxPrice) Return the first ChildShipment filtered by the invoiceMaxPrice column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByNotes(string $notes) Return the first ChildShipment filtered by the notes column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByBankcontact(string $bankContact) Return the first ChildShipment filtered by the bankContact column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByInsured(string $insured) Return the first ChildShipment filtered by the insured column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByExtrabruto(string $extraBruto) Return the first ChildShipment filtered by the extraBruto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByShippingcost(string $shippingCost) Return the first ChildShipment filtered by the shippingCost column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByCurrency(string $currency) Return the first ChildShipment filtered by the currency column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByPriceper(string $pricePer) Return the first ChildShipment filtered by the pricePer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByCustomname(string $customName) Return the first ChildShipment filtered by the customName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByGenadd(string $genAdd) Return the first ChildShipment filtered by the genAdd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByShipmentagend(string $shipmentAgend) Return the first ChildShipment filtered by the shipmentAgend column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShipment requireOneByBankId(int $bank_id) Return the first ChildShipment filtered by the bank_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShipment[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildShipment objects based on current ModelCriteria
 * @method     ChildShipment[]|ObjectCollection findById(int $id) Return ChildShipment objects filtered by the id column
 * @method     ChildShipment[]|ObjectCollection findBySupplierdateload(string $supplierDateLoad) Return ChildShipment objects filtered by the supplierDateLoad column
 * @method     ChildShipment[]|ObjectCollection findByBoatdateload(string $boatDateLoad) Return ChildShipment objects filtered by the boatDateLoad column
 * @method     ChildShipment[]|ObjectCollection findByLoadport(string $loadPort) Return ChildShipment objects filtered by the loadPort column
 * @method     ChildShipment[]|ObjectCollection findByDestinationport(string $destinationPort) Return ChildShipment objects filtered by the destinationPort column
 * @method     ChildShipment[]|ObjectCollection findByWeekno(string $weekNo) Return ChildShipment objects filtered by the weekNo column
 * @method     ChildShipment[]|ObjectCollection findByNoofcontainer(string $noOfContainer) Return ChildShipment objects filtered by the noOfContainer column
 * @method     ChildShipment[]|ObjectCollection findByExporter(string $exporter) Return ChildShipment objects filtered by the exporter column
 * @method     ChildShipment[]|ObjectCollection findByImporter(string $importer) Return ChildShipment objects filtered by the importer column
 * @method     ChildShipment[]|ObjectCollection findByNote(string $note) Return ChildShipment objects filtered by the note column
 * @method     ChildShipment[]|ObjectCollection findByCashoption(string $cashOption) Return ChildShipment objects filtered by the cashOption column
 * @method     ChildShipment[]|ObjectCollection findByInvoicemaxprice(string $invoiceMaxPrice) Return ChildShipment objects filtered by the invoiceMaxPrice column
 * @method     ChildShipment[]|ObjectCollection findByNotes(string $notes) Return ChildShipment objects filtered by the notes column
 * @method     ChildShipment[]|ObjectCollection findByBankcontact(string $bankContact) Return ChildShipment objects filtered by the bankContact column
 * @method     ChildShipment[]|ObjectCollection findByInsured(string $insured) Return ChildShipment objects filtered by the insured column
 * @method     ChildShipment[]|ObjectCollection findByExtrabruto(string $extraBruto) Return ChildShipment objects filtered by the extraBruto column
 * @method     ChildShipment[]|ObjectCollection findByShippingcost(string $shippingCost) Return ChildShipment objects filtered by the shippingCost column
 * @method     ChildShipment[]|ObjectCollection findByCurrency(string $currency) Return ChildShipment objects filtered by the currency column
 * @method     ChildShipment[]|ObjectCollection findByPriceper(string $pricePer) Return ChildShipment objects filtered by the pricePer column
 * @method     ChildShipment[]|ObjectCollection findByCustomname(string $customName) Return ChildShipment objects filtered by the customName column
 * @method     ChildShipment[]|ObjectCollection findByGenadd(string $genAdd) Return ChildShipment objects filtered by the genAdd column
 * @method     ChildShipment[]|ObjectCollection findByShipmentagend(string $shipmentAgend) Return ChildShipment objects filtered by the shipmentAgend column
 * @method     ChildShipment[]|ObjectCollection findByBankId(int $bank_id) Return ChildShipment objects filtered by the bank_id column
 * @method     ChildShipment[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ShipmentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \data_models\Base\ShipmentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\data_models\\Shipment', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildShipmentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildShipmentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildShipmentQuery) {
            return $criteria;
        }
        $query = new ChildShipmentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildShipment|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ShipmentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ShipmentTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildShipment A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, supplierDateLoad, boatDateLoad, loadPort, destinationPort, weekNo, noOfContainer, exporter, importer, note, cashOption, invoiceMaxPrice, notes, bankContact, insured, extraBruto, shippingCost, currency, pricePer, customName, genAdd, shipmentAgend, bank_id FROM shipment WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildShipment $obj */
            $obj = new ChildShipment();
            $obj->hydrate($row);
            ShipmentTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildShipment|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ShipmentTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ShipmentTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ShipmentTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ShipmentTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the supplierDateLoad column
     *
     * Example usage:
     * <code>
     * $query->filterBySupplierdateload('fooValue');   // WHERE supplierDateLoad = 'fooValue'
     * $query->filterBySupplierdateload('%fooValue%', Criteria::LIKE); // WHERE supplierDateLoad LIKE '%fooValue%'
     * </code>
     *
     * @param     string $supplierdateload The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterBySupplierdateload($supplierdateload = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($supplierdateload)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_SUPPLIERDATELOAD, $supplierdateload, $comparison);
    }

    /**
     * Filter the query on the boatDateLoad column
     *
     * Example usage:
     * <code>
     * $query->filterByBoatdateload('fooValue');   // WHERE boatDateLoad = 'fooValue'
     * $query->filterByBoatdateload('%fooValue%', Criteria::LIKE); // WHERE boatDateLoad LIKE '%fooValue%'
     * </code>
     *
     * @param     string $boatdateload The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByBoatdateload($boatdateload = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($boatdateload)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_BOATDATELOAD, $boatdateload, $comparison);
    }

    /**
     * Filter the query on the loadPort column
     *
     * Example usage:
     * <code>
     * $query->filterByLoadport('fooValue');   // WHERE loadPort = 'fooValue'
     * $query->filterByLoadport('%fooValue%', Criteria::LIKE); // WHERE loadPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $loadport The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByLoadport($loadport = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($loadport)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_LOADPORT, $loadport, $comparison);
    }

    /**
     * Filter the query on the destinationPort column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationport('fooValue');   // WHERE destinationPort = 'fooValue'
     * $query->filterByDestinationport('%fooValue%', Criteria::LIKE); // WHERE destinationPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationport The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByDestinationport($destinationport = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationport)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_DESTINATIONPORT, $destinationport, $comparison);
    }

    /**
     * Filter the query on the weekNo column
     *
     * Example usage:
     * <code>
     * $query->filterByWeekno('fooValue');   // WHERE weekNo = 'fooValue'
     * $query->filterByWeekno('%fooValue%', Criteria::LIKE); // WHERE weekNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $weekno The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByWeekno($weekno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($weekno)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_WEEKNO, $weekno, $comparison);
    }

    /**
     * Filter the query on the noOfContainer column
     *
     * Example usage:
     * <code>
     * $query->filterByNoofcontainer('fooValue');   // WHERE noOfContainer = 'fooValue'
     * $query->filterByNoofcontainer('%fooValue%', Criteria::LIKE); // WHERE noOfContainer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noofcontainer The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByNoofcontainer($noofcontainer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noofcontainer)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_NOOFCONTAINER, $noofcontainer, $comparison);
    }

    /**
     * Filter the query on the exporter column
     *
     * Example usage:
     * <code>
     * $query->filterByExporter('fooValue');   // WHERE exporter = 'fooValue'
     * $query->filterByExporter('%fooValue%', Criteria::LIKE); // WHERE exporter LIKE '%fooValue%'
     * </code>
     *
     * @param     string $exporter The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByExporter($exporter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($exporter)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_EXPORTER, $exporter, $comparison);
    }

    /**
     * Filter the query on the importer column
     *
     * Example usage:
     * <code>
     * $query->filterByImporter('fooValue');   // WHERE importer = 'fooValue'
     * $query->filterByImporter('%fooValue%', Criteria::LIKE); // WHERE importer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $importer The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByImporter($importer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($importer)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_IMPORTER, $importer, $comparison);
    }

    /**
     * Filter the query on the note column
     *
     * Example usage:
     * <code>
     * $query->filterByNote('fooValue');   // WHERE note = 'fooValue'
     * $query->filterByNote('%fooValue%', Criteria::LIKE); // WHERE note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $note The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByNote($note = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($note)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_NOTE, $note, $comparison);
    }

    /**
     * Filter the query on the cashOption column
     *
     * Example usage:
     * <code>
     * $query->filterByCashoption('fooValue');   // WHERE cashOption = 'fooValue'
     * $query->filterByCashoption('%fooValue%', Criteria::LIKE); // WHERE cashOption LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cashoption The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByCashoption($cashoption = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cashoption)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_CASHOPTION, $cashoption, $comparison);
    }

    /**
     * Filter the query on the invoiceMaxPrice column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoicemaxprice('fooValue');   // WHERE invoiceMaxPrice = 'fooValue'
     * $query->filterByInvoicemaxprice('%fooValue%', Criteria::LIKE); // WHERE invoiceMaxPrice LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoicemaxprice The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByInvoicemaxprice($invoicemaxprice = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoicemaxprice)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_INVOICEMAXPRICE, $invoicemaxprice, $comparison);
    }

    /**
     * Filter the query on the notes column
     *
     * Example usage:
     * <code>
     * $query->filterByNotes('fooValue');   // WHERE notes = 'fooValue'
     * $query->filterByNotes('%fooValue%', Criteria::LIKE); // WHERE notes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $notes The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByNotes($notes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($notes)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_NOTES, $notes, $comparison);
    }

    /**
     * Filter the query on the bankContact column
     *
     * Example usage:
     * <code>
     * $query->filterByBankcontact('fooValue');   // WHERE bankContact = 'fooValue'
     * $query->filterByBankcontact('%fooValue%', Criteria::LIKE); // WHERE bankContact LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bankcontact The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByBankcontact($bankcontact = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bankcontact)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_BANKCONTACT, $bankcontact, $comparison);
    }

    /**
     * Filter the query on the insured column
     *
     * Example usage:
     * <code>
     * $query->filterByInsured('fooValue');   // WHERE insured = 'fooValue'
     * $query->filterByInsured('%fooValue%', Criteria::LIKE); // WHERE insured LIKE '%fooValue%'
     * </code>
     *
     * @param     string $insured The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByInsured($insured = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($insured)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_INSURED, $insured, $comparison);
    }

    /**
     * Filter the query on the extraBruto column
     *
     * Example usage:
     * <code>
     * $query->filterByExtrabruto('fooValue');   // WHERE extraBruto = 'fooValue'
     * $query->filterByExtrabruto('%fooValue%', Criteria::LIKE); // WHERE extraBruto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extrabruto The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByExtrabruto($extrabruto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extrabruto)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_EXTRABRUTO, $extrabruto, $comparison);
    }

    /**
     * Filter the query on the shippingCost column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingcost('fooValue');   // WHERE shippingCost = 'fooValue'
     * $query->filterByShippingcost('%fooValue%', Criteria::LIKE); // WHERE shippingCost LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shippingcost The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByShippingcost($shippingcost = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shippingcost)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_SHIPPINGCOST, $shippingcost, $comparison);
    }

    /**
     * Filter the query on the currency column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrency('fooValue');   // WHERE currency = 'fooValue'
     * $query->filterByCurrency('%fooValue%', Criteria::LIKE); // WHERE currency LIKE '%fooValue%'
     * </code>
     *
     * @param     string $currency The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByCurrency($currency = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($currency)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_CURRENCY, $currency, $comparison);
    }

    /**
     * Filter the query on the pricePer column
     *
     * Example usage:
     * <code>
     * $query->filterByPriceper('fooValue');   // WHERE pricePer = 'fooValue'
     * $query->filterByPriceper('%fooValue%', Criteria::LIKE); // WHERE pricePer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $priceper The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByPriceper($priceper = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($priceper)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_PRICEPER, $priceper, $comparison);
    }

    /**
     * Filter the query on the customName column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomname('fooValue');   // WHERE customName = 'fooValue'
     * $query->filterByCustomname('%fooValue%', Criteria::LIKE); // WHERE customName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByCustomname($customname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_CUSTOMNAME, $customname, $comparison);
    }

    /**
     * Filter the query on the genAdd column
     *
     * Example usage:
     * <code>
     * $query->filterByGenadd('fooValue');   // WHERE genAdd = 'fooValue'
     * $query->filterByGenadd('%fooValue%', Criteria::LIKE); // WHERE genAdd LIKE '%fooValue%'
     * </code>
     *
     * @param     string $genadd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByGenadd($genadd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($genadd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_GENADD, $genadd, $comparison);
    }

    /**
     * Filter the query on the shipmentAgend column
     *
     * Example usage:
     * <code>
     * $query->filterByShipmentagend('fooValue');   // WHERE shipmentAgend = 'fooValue'
     * $query->filterByShipmentagend('%fooValue%', Criteria::LIKE); // WHERE shipmentAgend LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shipmentagend The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByShipmentagend($shipmentagend = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shipmentagend)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_SHIPMENTAGEND, $shipmentagend, $comparison);
    }

    /**
     * Filter the query on the bank_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBankId(1234); // WHERE bank_id = 1234
     * $query->filterByBankId(array(12, 34)); // WHERE bank_id IN (12, 34)
     * $query->filterByBankId(array('min' => 12)); // WHERE bank_id > 12
     * </code>
     *
     * @see       filterByBank()
     *
     * @param     mixed $bankId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByBankId($bankId = null, $comparison = null)
    {
        if (is_array($bankId)) {
            $useMinMax = false;
            if (isset($bankId['min'])) {
                $this->addUsingAlias(ShipmentTableMap::COL_BANK_ID, $bankId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bankId['max'])) {
                $this->addUsingAlias(ShipmentTableMap::COL_BANK_ID, $bankId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShipmentTableMap::COL_BANK_ID, $bankId, $comparison);
    }

    /**
     * Filter the query by a related \data_models\Bank object
     *
     * @param \data_models\Bank|ObjectCollection $bank The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByBank($bank, $comparison = null)
    {
        if ($bank instanceof \data_models\Bank) {
            return $this
                ->addUsingAlias(ShipmentTableMap::COL_BANK_ID, $bank->getId(), $comparison);
        } elseif ($bank instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ShipmentTableMap::COL_BANK_ID, $bank->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBank() only accepts arguments of type \data_models\Bank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Bank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function joinBank($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Bank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Bank');
        }

        return $this;
    }

    /**
     * Use the Bank relation Bank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \data_models\BankQuery A secondary query class using the current class as primary query
     */
    public function useBankQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Bank', '\data_models\BankQuery');
    }

    /**
     * Filter the query by a related \data_models\Product object
     *
     * @param \data_models\Product|ObjectCollection $product the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildShipmentQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \data_models\Product) {
            return $this
                ->addUsingAlias(ShipmentTableMap::COL_ID, $product->getShipmentId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            return $this
                ->useProductQuery()
                ->filterByPrimaryKeys($product->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \data_models\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \data_models\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\data_models\ProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildShipment $shipment Object to remove from the list of results
     *
     * @return $this|ChildShipmentQuery The current query, for fluid interface
     */
    public function prune($shipment = null)
    {
        if ($shipment) {
            $this->addUsingAlias(ShipmentTableMap::COL_ID, $shipment->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the shipment table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShipmentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ShipmentTableMap::clearInstancePool();
            ShipmentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShipmentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ShipmentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ShipmentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ShipmentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ShipmentQuery
