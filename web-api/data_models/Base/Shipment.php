<?php

namespace data_models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use data_models\Bank as ChildBank;
use data_models\BankQuery as ChildBankQuery;
use data_models\Product as ChildProduct;
use data_models\ProductQuery as ChildProductQuery;
use data_models\Shipment as ChildShipment;
use data_models\ShipmentQuery as ChildShipmentQuery;
use data_models\Map\ProductTableMap;
use data_models\Map\ShipmentTableMap;

/**
 * Base class that represents a row from the 'shipment' table.
 *
 *
 *
 * @package    propel.generator.data_models.Base
 */
abstract class Shipment implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\data_models\\Map\\ShipmentTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the supplierdateload field.
     *
     * @var        string
     */
    protected $supplierdateload;

    /**
     * The value for the boatdateload field.
     *
     * @var        string
     */
    protected $boatdateload;

    /**
     * The value for the loadport field.
     *
     * @var        string
     */
    protected $loadport;

    /**
     * The value for the destinationport field.
     *
     * @var        string
     */
    protected $destinationport;

    /**
     * The value for the weekno field.
     *
     * @var        string
     */
    protected $weekno;

    /**
     * The value for the noofcontainer field.
     *
     * @var        string
     */
    protected $noofcontainer;

    /**
     * The value for the exporter field.
     *
     * @var        string
     */
    protected $exporter;

    /**
     * The value for the importer field.
     *
     * @var        string
     */
    protected $importer;

    /**
     * The value for the note field.
     *
     * @var        string
     */
    protected $note;

    /**
     * The value for the cashoption field.
     *
     * @var        string
     */
    protected $cashoption;

    /**
     * The value for the invoicemaxprice field.
     *
     * @var        string
     */
    protected $invoicemaxprice;

    /**
     * The value for the notes field.
     *
     * @var        string
     */
    protected $notes;

    /**
     * The value for the bankcontact field.
     *
     * @var        string
     */
    protected $bankcontact;

    /**
     * The value for the insured field.
     *
     * @var        string
     */
    protected $insured;

    /**
     * The value for the extrabruto field.
     *
     * @var        string
     */
    protected $extrabruto;

    /**
     * The value for the shippingcost field.
     *
     * @var        string
     */
    protected $shippingcost;

    /**
     * The value for the currency field.
     *
     * @var        string
     */
    protected $currency;

    /**
     * The value for the priceper field.
     *
     * @var        string
     */
    protected $priceper;

    /**
     * The value for the customname field.
     *
     * @var        string
     */
    protected $customname;

    /**
     * The value for the genadd field.
     *
     * @var        string
     */
    protected $genadd;

    /**
     * The value for the shipmentagend field.
     *
     * @var        string
     */
    protected $shipmentagend;

    /**
     * The value for the bank_id field.
     *
     * @var        int
     */
    protected $bank_id;

    /**
     * @var        ChildBank
     */
    protected $aBank;

    /**
     * @var        ObjectCollection|ChildProduct[] Collection to store aggregation of ChildProduct objects.
     */
    protected $collProducts;
    protected $collProductsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProduct[]
     */
    protected $productsScheduledForDeletion = null;

    /**
     * Initializes internal state of data_models\Base\Shipment object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Shipment</code> instance.  If
     * <code>obj</code> is an instance of <code>Shipment</code>, delegates to
     * <code>equals(Shipment)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Shipment The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [supplierdateload] column value.
     *
     * @return string
     */
    public function getSupplierdateload()
    {
        return $this->supplierdateload;
    }

    /**
     * Get the [boatdateload] column value.
     *
     * @return string
     */
    public function getBoatdateload()
    {
        return $this->boatdateload;
    }

    /**
     * Get the [loadport] column value.
     *
     * @return string
     */
    public function getLoadport()
    {
        return $this->loadport;
    }

    /**
     * Get the [destinationport] column value.
     *
     * @return string
     */
    public function getDestinationport()
    {
        return $this->destinationport;
    }

    /**
     * Get the [weekno] column value.
     *
     * @return string
     */
    public function getWeekno()
    {
        return $this->weekno;
    }

    /**
     * Get the [noofcontainer] column value.
     *
     * @return string
     */
    public function getNoofcontainer()
    {
        return $this->noofcontainer;
    }

    /**
     * Get the [exporter] column value.
     *
     * @return string
     */
    public function getExporter()
    {
        return $this->exporter;
    }

    /**
     * Get the [importer] column value.
     *
     * @return string
     */
    public function getImporter()
    {
        return $this->importer;
    }

    /**
     * Get the [note] column value.
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Get the [cashoption] column value.
     *
     * @return string
     */
    public function getCashoption()
    {
        return $this->cashoption;
    }

    /**
     * Get the [invoicemaxprice] column value.
     *
     * @return string
     */
    public function getInvoicemaxprice()
    {
        return $this->invoicemaxprice;
    }

    /**
     * Get the [notes] column value.
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Get the [bankcontact] column value.
     *
     * @return string
     */
    public function getBankcontact()
    {
        return $this->bankcontact;
    }

    /**
     * Get the [insured] column value.
     *
     * @return string
     */
    public function getInsured()
    {
        return $this->insured;
    }

    /**
     * Get the [extrabruto] column value.
     *
     * @return string
     */
    public function getExtrabruto()
    {
        return $this->extrabruto;
    }

    /**
     * Get the [shippingcost] column value.
     *
     * @return string
     */
    public function getShippingcost()
    {
        return $this->shippingcost;
    }

    /**
     * Get the [currency] column value.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Get the [priceper] column value.
     *
     * @return string
     */
    public function getPriceper()
    {
        return $this->priceper;
    }

    /**
     * Get the [customname] column value.
     *
     * @return string
     */
    public function getCustomname()
    {
        return $this->customname;
    }

    /**
     * Get the [genadd] column value.
     *
     * @return string
     */
    public function getGenadd()
    {
        return $this->genadd;
    }

    /**
     * Get the [shipmentagend] column value.
     *
     * @return string
     */
    public function getShipmentagend()
    {
        return $this->shipmentagend;
    }

    /**
     * Get the [bank_id] column value.
     *
     * @return int
     */
    public function getBankId()
    {
        return $this->bank_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [supplierdateload] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setSupplierdateload($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->supplierdateload !== $v) {
            $this->supplierdateload = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_SUPPLIERDATELOAD] = true;
        }

        return $this;
    } // setSupplierdateload()

    /**
     * Set the value of [boatdateload] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setBoatdateload($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->boatdateload !== $v) {
            $this->boatdateload = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_BOATDATELOAD] = true;
        }

        return $this;
    } // setBoatdateload()

    /**
     * Set the value of [loadport] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setLoadport($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->loadport !== $v) {
            $this->loadport = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_LOADPORT] = true;
        }

        return $this;
    } // setLoadport()

    /**
     * Set the value of [destinationport] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setDestinationport($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->destinationport !== $v) {
            $this->destinationport = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_DESTINATIONPORT] = true;
        }

        return $this;
    } // setDestinationport()

    /**
     * Set the value of [weekno] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setWeekno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->weekno !== $v) {
            $this->weekno = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_WEEKNO] = true;
        }

        return $this;
    } // setWeekno()

    /**
     * Set the value of [noofcontainer] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setNoofcontainer($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->noofcontainer !== $v) {
            $this->noofcontainer = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_NOOFCONTAINER] = true;
        }

        return $this;
    } // setNoofcontainer()

    /**
     * Set the value of [exporter] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setExporter($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->exporter !== $v) {
            $this->exporter = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_EXPORTER] = true;
        }

        return $this;
    } // setExporter()

    /**
     * Set the value of [importer] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setImporter($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->importer !== $v) {
            $this->importer = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_IMPORTER] = true;
        }

        return $this;
    } // setImporter()

    /**
     * Set the value of [note] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setNote($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->note !== $v) {
            $this->note = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_NOTE] = true;
        }

        return $this;
    } // setNote()

    /**
     * Set the value of [cashoption] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setCashoption($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cashoption !== $v) {
            $this->cashoption = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_CASHOPTION] = true;
        }

        return $this;
    } // setCashoption()

    /**
     * Set the value of [invoicemaxprice] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setInvoicemaxprice($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoicemaxprice !== $v) {
            $this->invoicemaxprice = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_INVOICEMAXPRICE] = true;
        }

        return $this;
    } // setInvoicemaxprice()

    /**
     * Set the value of [notes] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setNotes($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->notes !== $v) {
            $this->notes = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_NOTES] = true;
        }

        return $this;
    } // setNotes()

    /**
     * Set the value of [bankcontact] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setBankcontact($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bankcontact !== $v) {
            $this->bankcontact = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_BANKCONTACT] = true;
        }

        return $this;
    } // setBankcontact()

    /**
     * Set the value of [insured] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setInsured($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->insured !== $v) {
            $this->insured = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_INSURED] = true;
        }

        return $this;
    } // setInsured()

    /**
     * Set the value of [extrabruto] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setExtrabruto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->extrabruto !== $v) {
            $this->extrabruto = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_EXTRABRUTO] = true;
        }

        return $this;
    } // setExtrabruto()

    /**
     * Set the value of [shippingcost] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setShippingcost($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shippingcost !== $v) {
            $this->shippingcost = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_SHIPPINGCOST] = true;
        }

        return $this;
    } // setShippingcost()

    /**
     * Set the value of [currency] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setCurrency($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->currency !== $v) {
            $this->currency = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_CURRENCY] = true;
        }

        return $this;
    } // setCurrency()

    /**
     * Set the value of [priceper] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setPriceper($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->priceper !== $v) {
            $this->priceper = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_PRICEPER] = true;
        }

        return $this;
    } // setPriceper()

    /**
     * Set the value of [customname] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setCustomname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customname !== $v) {
            $this->customname = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_CUSTOMNAME] = true;
        }

        return $this;
    } // setCustomname()

    /**
     * Set the value of [genadd] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setGenadd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->genadd !== $v) {
            $this->genadd = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_GENADD] = true;
        }

        return $this;
    } // setGenadd()

    /**
     * Set the value of [shipmentagend] column.
     *
     * @param string $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setShipmentagend($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shipmentagend !== $v) {
            $this->shipmentagend = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_SHIPMENTAGEND] = true;
        }

        return $this;
    } // setShipmentagend()

    /**
     * Set the value of [bank_id] column.
     *
     * @param int $v new value
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function setBankId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->bank_id !== $v) {
            $this->bank_id = $v;
            $this->modifiedColumns[ShipmentTableMap::COL_BANK_ID] = true;
        }

        if ($this->aBank !== null && $this->aBank->getId() !== $v) {
            $this->aBank = null;
        }

        return $this;
    } // setBankId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ShipmentTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ShipmentTableMap::translateFieldName('Supplierdateload', TableMap::TYPE_PHPNAME, $indexType)];
            $this->supplierdateload = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ShipmentTableMap::translateFieldName('Boatdateload', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boatdateload = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ShipmentTableMap::translateFieldName('Loadport', TableMap::TYPE_PHPNAME, $indexType)];
            $this->loadport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ShipmentTableMap::translateFieldName('Destinationport', TableMap::TYPE_PHPNAME, $indexType)];
            $this->destinationport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ShipmentTableMap::translateFieldName('Weekno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weekno = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ShipmentTableMap::translateFieldName('Noofcontainer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->noofcontainer = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ShipmentTableMap::translateFieldName('Exporter', TableMap::TYPE_PHPNAME, $indexType)];
            $this->exporter = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ShipmentTableMap::translateFieldName('Importer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->importer = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ShipmentTableMap::translateFieldName('Note', TableMap::TYPE_PHPNAME, $indexType)];
            $this->note = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ShipmentTableMap::translateFieldName('Cashoption', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cashoption = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ShipmentTableMap::translateFieldName('Invoicemaxprice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoicemaxprice = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ShipmentTableMap::translateFieldName('Notes', TableMap::TYPE_PHPNAME, $indexType)];
            $this->notes = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ShipmentTableMap::translateFieldName('Bankcontact', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bankcontact = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ShipmentTableMap::translateFieldName('Insured', TableMap::TYPE_PHPNAME, $indexType)];
            $this->insured = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ShipmentTableMap::translateFieldName('Extrabruto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->extrabruto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ShipmentTableMap::translateFieldName('Shippingcost', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shippingcost = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ShipmentTableMap::translateFieldName('Currency', TableMap::TYPE_PHPNAME, $indexType)];
            $this->currency = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ShipmentTableMap::translateFieldName('Priceper', TableMap::TYPE_PHPNAME, $indexType)];
            $this->priceper = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ShipmentTableMap::translateFieldName('Customname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ShipmentTableMap::translateFieldName('Genadd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->genadd = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ShipmentTableMap::translateFieldName('Shipmentagend', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipmentagend = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ShipmentTableMap::translateFieldName('BankId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bank_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 23; // 23 = ShipmentTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\data_models\\Shipment'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBank !== null && $this->bank_id !== $this->aBank->getId()) {
            $this->aBank = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ShipmentTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildShipmentQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBank = null;
            $this->collProducts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Shipment::setDeleted()
     * @see Shipment::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShipmentTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildShipmentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShipmentTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ShipmentTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBank !== null) {
                if ($this->aBank->isModified() || $this->aBank->isNew()) {
                    $affectedRows += $this->aBank->save($con);
                }
                $this->setBank($this->aBank);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->productsScheduledForDeletion !== null) {
                if (!$this->productsScheduledForDeletion->isEmpty()) {
                    \data_models\ProductQuery::create()
                        ->filterByPrimaryKeys($this->productsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productsScheduledForDeletion = null;
                }
            }

            if ($this->collProducts !== null) {
                foreach ($this->collProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ShipmentTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ShipmentTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ShipmentTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_SUPPLIERDATELOAD)) {
            $modifiedColumns[':p' . $index++]  = 'supplierDateLoad';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_BOATDATELOAD)) {
            $modifiedColumns[':p' . $index++]  = 'boatDateLoad';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_LOADPORT)) {
            $modifiedColumns[':p' . $index++]  = 'loadPort';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_DESTINATIONPORT)) {
            $modifiedColumns[':p' . $index++]  = 'destinationPort';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_WEEKNO)) {
            $modifiedColumns[':p' . $index++]  = 'weekNo';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_NOOFCONTAINER)) {
            $modifiedColumns[':p' . $index++]  = 'noOfContainer';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_EXPORTER)) {
            $modifiedColumns[':p' . $index++]  = 'exporter';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_IMPORTER)) {
            $modifiedColumns[':p' . $index++]  = 'importer';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_NOTE)) {
            $modifiedColumns[':p' . $index++]  = 'note';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_CASHOPTION)) {
            $modifiedColumns[':p' . $index++]  = 'cashOption';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_INVOICEMAXPRICE)) {
            $modifiedColumns[':p' . $index++]  = 'invoiceMaxPrice';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_NOTES)) {
            $modifiedColumns[':p' . $index++]  = 'notes';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_BANKCONTACT)) {
            $modifiedColumns[':p' . $index++]  = 'bankContact';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_INSURED)) {
            $modifiedColumns[':p' . $index++]  = 'insured';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_EXTRABRUTO)) {
            $modifiedColumns[':p' . $index++]  = 'extraBruto';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_SHIPPINGCOST)) {
            $modifiedColumns[':p' . $index++]  = 'shippingCost';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_CURRENCY)) {
            $modifiedColumns[':p' . $index++]  = 'currency';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_PRICEPER)) {
            $modifiedColumns[':p' . $index++]  = 'pricePer';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_CUSTOMNAME)) {
            $modifiedColumns[':p' . $index++]  = 'customName';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_GENADD)) {
            $modifiedColumns[':p' . $index++]  = 'genAdd';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_SHIPMENTAGEND)) {
            $modifiedColumns[':p' . $index++]  = 'shipmentAgend';
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_BANK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'bank_id';
        }

        $sql = sprintf(
            'INSERT INTO shipment (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'supplierDateLoad':
                        $stmt->bindValue($identifier, $this->supplierdateload, PDO::PARAM_STR);
                        break;
                    case 'boatDateLoad':
                        $stmt->bindValue($identifier, $this->boatdateload, PDO::PARAM_STR);
                        break;
                    case 'loadPort':
                        $stmt->bindValue($identifier, $this->loadport, PDO::PARAM_STR);
                        break;
                    case 'destinationPort':
                        $stmt->bindValue($identifier, $this->destinationport, PDO::PARAM_STR);
                        break;
                    case 'weekNo':
                        $stmt->bindValue($identifier, $this->weekno, PDO::PARAM_STR);
                        break;
                    case 'noOfContainer':
                        $stmt->bindValue($identifier, $this->noofcontainer, PDO::PARAM_STR);
                        break;
                    case 'exporter':
                        $stmt->bindValue($identifier, $this->exporter, PDO::PARAM_STR);
                        break;
                    case 'importer':
                        $stmt->bindValue($identifier, $this->importer, PDO::PARAM_STR);
                        break;
                    case 'note':
                        $stmt->bindValue($identifier, $this->note, PDO::PARAM_STR);
                        break;
                    case 'cashOption':
                        $stmt->bindValue($identifier, $this->cashoption, PDO::PARAM_STR);
                        break;
                    case 'invoiceMaxPrice':
                        $stmt->bindValue($identifier, $this->invoicemaxprice, PDO::PARAM_STR);
                        break;
                    case 'notes':
                        $stmt->bindValue($identifier, $this->notes, PDO::PARAM_STR);
                        break;
                    case 'bankContact':
                        $stmt->bindValue($identifier, $this->bankcontact, PDO::PARAM_STR);
                        break;
                    case 'insured':
                        $stmt->bindValue($identifier, $this->insured, PDO::PARAM_STR);
                        break;
                    case 'extraBruto':
                        $stmt->bindValue($identifier, $this->extrabruto, PDO::PARAM_STR);
                        break;
                    case 'shippingCost':
                        $stmt->bindValue($identifier, $this->shippingcost, PDO::PARAM_STR);
                        break;
                    case 'currency':
                        $stmt->bindValue($identifier, $this->currency, PDO::PARAM_STR);
                        break;
                    case 'pricePer':
                        $stmt->bindValue($identifier, $this->priceper, PDO::PARAM_STR);
                        break;
                    case 'customName':
                        $stmt->bindValue($identifier, $this->customname, PDO::PARAM_STR);
                        break;
                    case 'genAdd':
                        $stmt->bindValue($identifier, $this->genadd, PDO::PARAM_STR);
                        break;
                    case 'shipmentAgend':
                        $stmt->bindValue($identifier, $this->shipmentagend, PDO::PARAM_STR);
                        break;
                    case 'bank_id':
                        $stmt->bindValue($identifier, $this->bank_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ShipmentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSupplierdateload();
                break;
            case 2:
                return $this->getBoatdateload();
                break;
            case 3:
                return $this->getLoadport();
                break;
            case 4:
                return $this->getDestinationport();
                break;
            case 5:
                return $this->getWeekno();
                break;
            case 6:
                return $this->getNoofcontainer();
                break;
            case 7:
                return $this->getExporter();
                break;
            case 8:
                return $this->getImporter();
                break;
            case 9:
                return $this->getNote();
                break;
            case 10:
                return $this->getCashoption();
                break;
            case 11:
                return $this->getInvoicemaxprice();
                break;
            case 12:
                return $this->getNotes();
                break;
            case 13:
                return $this->getBankcontact();
                break;
            case 14:
                return $this->getInsured();
                break;
            case 15:
                return $this->getExtrabruto();
                break;
            case 16:
                return $this->getShippingcost();
                break;
            case 17:
                return $this->getCurrency();
                break;
            case 18:
                return $this->getPriceper();
                break;
            case 19:
                return $this->getCustomname();
                break;
            case 20:
                return $this->getGenadd();
                break;
            case 21:
                return $this->getShipmentagend();
                break;
            case 22:
                return $this->getBankId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Shipment'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Shipment'][$this->hashCode()] = true;
        $keys = ShipmentTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSupplierdateload(),
            $keys[2] => $this->getBoatdateload(),
            $keys[3] => $this->getLoadport(),
            $keys[4] => $this->getDestinationport(),
            $keys[5] => $this->getWeekno(),
            $keys[6] => $this->getNoofcontainer(),
            $keys[7] => $this->getExporter(),
            $keys[8] => $this->getImporter(),
            $keys[9] => $this->getNote(),
            $keys[10] => $this->getCashoption(),
            $keys[11] => $this->getInvoicemaxprice(),
            $keys[12] => $this->getNotes(),
            $keys[13] => $this->getBankcontact(),
            $keys[14] => $this->getInsured(),
            $keys[15] => $this->getExtrabruto(),
            $keys[16] => $this->getShippingcost(),
            $keys[17] => $this->getCurrency(),
            $keys[18] => $this->getPriceper(),
            $keys[19] => $this->getCustomname(),
            $keys[20] => $this->getGenadd(),
            $keys[21] => $this->getShipmentagend(),
            $keys[22] => $this->getBankId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBank) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bank';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bank';
                        break;
                    default:
                        $key = 'Bank';
                }

                $result[$key] = $this->aBank->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collProducts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'products';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'products';
                        break;
                    default:
                        $key = 'Products';
                }

                $result[$key] = $this->collProducts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\data_models\Shipment
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ShipmentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\data_models\Shipment
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSupplierdateload($value);
                break;
            case 2:
                $this->setBoatdateload($value);
                break;
            case 3:
                $this->setLoadport($value);
                break;
            case 4:
                $this->setDestinationport($value);
                break;
            case 5:
                $this->setWeekno($value);
                break;
            case 6:
                $this->setNoofcontainer($value);
                break;
            case 7:
                $this->setExporter($value);
                break;
            case 8:
                $this->setImporter($value);
                break;
            case 9:
                $this->setNote($value);
                break;
            case 10:
                $this->setCashoption($value);
                break;
            case 11:
                $this->setInvoicemaxprice($value);
                break;
            case 12:
                $this->setNotes($value);
                break;
            case 13:
                $this->setBankcontact($value);
                break;
            case 14:
                $this->setInsured($value);
                break;
            case 15:
                $this->setExtrabruto($value);
                break;
            case 16:
                $this->setShippingcost($value);
                break;
            case 17:
                $this->setCurrency($value);
                break;
            case 18:
                $this->setPriceper($value);
                break;
            case 19:
                $this->setCustomname($value);
                break;
            case 20:
                $this->setGenadd($value);
                break;
            case 21:
                $this->setShipmentagend($value);
                break;
            case 22:
                $this->setBankId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ShipmentTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSupplierdateload($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBoatdateload($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setLoadport($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDestinationport($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setWeekno($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setNoofcontainer($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setExporter($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setImporter($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setNote($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCashoption($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setInvoicemaxprice($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setNotes($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setBankcontact($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setInsured($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setExtrabruto($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setShippingcost($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCurrency($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setPriceper($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setCustomname($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setGenadd($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setShipmentagend($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setBankId($arr[$keys[22]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\data_models\Shipment The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ShipmentTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ShipmentTableMap::COL_ID)) {
            $criteria->add(ShipmentTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_SUPPLIERDATELOAD)) {
            $criteria->add(ShipmentTableMap::COL_SUPPLIERDATELOAD, $this->supplierdateload);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_BOATDATELOAD)) {
            $criteria->add(ShipmentTableMap::COL_BOATDATELOAD, $this->boatdateload);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_LOADPORT)) {
            $criteria->add(ShipmentTableMap::COL_LOADPORT, $this->loadport);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_DESTINATIONPORT)) {
            $criteria->add(ShipmentTableMap::COL_DESTINATIONPORT, $this->destinationport);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_WEEKNO)) {
            $criteria->add(ShipmentTableMap::COL_WEEKNO, $this->weekno);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_NOOFCONTAINER)) {
            $criteria->add(ShipmentTableMap::COL_NOOFCONTAINER, $this->noofcontainer);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_EXPORTER)) {
            $criteria->add(ShipmentTableMap::COL_EXPORTER, $this->exporter);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_IMPORTER)) {
            $criteria->add(ShipmentTableMap::COL_IMPORTER, $this->importer);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_NOTE)) {
            $criteria->add(ShipmentTableMap::COL_NOTE, $this->note);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_CASHOPTION)) {
            $criteria->add(ShipmentTableMap::COL_CASHOPTION, $this->cashoption);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_INVOICEMAXPRICE)) {
            $criteria->add(ShipmentTableMap::COL_INVOICEMAXPRICE, $this->invoicemaxprice);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_NOTES)) {
            $criteria->add(ShipmentTableMap::COL_NOTES, $this->notes);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_BANKCONTACT)) {
            $criteria->add(ShipmentTableMap::COL_BANKCONTACT, $this->bankcontact);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_INSURED)) {
            $criteria->add(ShipmentTableMap::COL_INSURED, $this->insured);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_EXTRABRUTO)) {
            $criteria->add(ShipmentTableMap::COL_EXTRABRUTO, $this->extrabruto);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_SHIPPINGCOST)) {
            $criteria->add(ShipmentTableMap::COL_SHIPPINGCOST, $this->shippingcost);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_CURRENCY)) {
            $criteria->add(ShipmentTableMap::COL_CURRENCY, $this->currency);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_PRICEPER)) {
            $criteria->add(ShipmentTableMap::COL_PRICEPER, $this->priceper);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_CUSTOMNAME)) {
            $criteria->add(ShipmentTableMap::COL_CUSTOMNAME, $this->customname);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_GENADD)) {
            $criteria->add(ShipmentTableMap::COL_GENADD, $this->genadd);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_SHIPMENTAGEND)) {
            $criteria->add(ShipmentTableMap::COL_SHIPMENTAGEND, $this->shipmentagend);
        }
        if ($this->isColumnModified(ShipmentTableMap::COL_BANK_ID)) {
            $criteria->add(ShipmentTableMap::COL_BANK_ID, $this->bank_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildShipmentQuery::create();
        $criteria->add(ShipmentTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \data_models\Shipment (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSupplierdateload($this->getSupplierdateload());
        $copyObj->setBoatdateload($this->getBoatdateload());
        $copyObj->setLoadport($this->getLoadport());
        $copyObj->setDestinationport($this->getDestinationport());
        $copyObj->setWeekno($this->getWeekno());
        $copyObj->setNoofcontainer($this->getNoofcontainer());
        $copyObj->setExporter($this->getExporter());
        $copyObj->setImporter($this->getImporter());
        $copyObj->setNote($this->getNote());
        $copyObj->setCashoption($this->getCashoption());
        $copyObj->setInvoicemaxprice($this->getInvoicemaxprice());
        $copyObj->setNotes($this->getNotes());
        $copyObj->setBankcontact($this->getBankcontact());
        $copyObj->setInsured($this->getInsured());
        $copyObj->setExtrabruto($this->getExtrabruto());
        $copyObj->setShippingcost($this->getShippingcost());
        $copyObj->setCurrency($this->getCurrency());
        $copyObj->setPriceper($this->getPriceper());
        $copyObj->setCustomname($this->getCustomname());
        $copyObj->setGenadd($this->getGenadd());
        $copyObj->setShipmentagend($this->getShipmentagend());
        $copyObj->setBankId($this->getBankId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProduct($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \data_models\Shipment Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBank object.
     *
     * @param  ChildBank $v
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBank(ChildBank $v = null)
    {
        if ($v === null) {
            $this->setBankId(NULL);
        } else {
            $this->setBankId($v->getId());
        }

        $this->aBank = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBank object, it will not be re-added.
        if ($v !== null) {
            $v->addShipment($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBank object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBank The associated ChildBank object.
     * @throws PropelException
     */
    public function getBank(ConnectionInterface $con = null)
    {
        if ($this->aBank === null && ($this->bank_id != 0)) {
            $this->aBank = ChildBankQuery::create()->findPk($this->bank_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBank->addShipments($this);
             */
        }

        return $this->aBank;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Product' == $relationName) {
            $this->initProducts();
            return;
        }
    }

    /**
     * Clears out the collProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProducts()
     */
    public function clearProducts()
    {
        $this->collProducts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProducts collection loaded partially.
     */
    public function resetPartialProducts($v = true)
    {
        $this->collProductsPartial = $v;
    }

    /**
     * Initializes the collProducts collection.
     *
     * By default this just sets the collProducts collection to an empty array (like clearcollProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProducts($overrideExisting = true)
    {
        if (null !== $this->collProducts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductTableMap::getTableMap()->getCollectionClassName();

        $this->collProducts = new $collectionClassName;
        $this->collProducts->setModel('\data_models\Product');
    }

    /**
     * Gets an array of ChildProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildShipment is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProduct[] List of ChildProduct objects
     * @throws PropelException
     */
    public function getProducts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductsPartial && !$this->isNew();
        if (null === $this->collProducts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProducts) {
                // return empty collection
                $this->initProducts();
            } else {
                $collProducts = ChildProductQuery::create(null, $criteria)
                    ->filterByShipment($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductsPartial && count($collProducts)) {
                        $this->initProducts(false);

                        foreach ($collProducts as $obj) {
                            if (false == $this->collProducts->contains($obj)) {
                                $this->collProducts->append($obj);
                            }
                        }

                        $this->collProductsPartial = true;
                    }

                    return $collProducts;
                }

                if ($partial && $this->collProducts) {
                    foreach ($this->collProducts as $obj) {
                        if ($obj->isNew()) {
                            $collProducts[] = $obj;
                        }
                    }
                }

                $this->collProducts = $collProducts;
                $this->collProductsPartial = false;
            }
        }

        return $this->collProducts;
    }

    /**
     * Sets a collection of ChildProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $products A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildShipment The current object (for fluent API support)
     */
    public function setProducts(Collection $products, ConnectionInterface $con = null)
    {
        /** @var ChildProduct[] $productsToDelete */
        $productsToDelete = $this->getProducts(new Criteria(), $con)->diff($products);


        $this->productsScheduledForDeletion = $productsToDelete;

        foreach ($productsToDelete as $productRemoved) {
            $productRemoved->setShipment(null);
        }

        $this->collProducts = null;
        foreach ($products as $product) {
            $this->addProduct($product);
        }

        $this->collProducts = $products;
        $this->collProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Product objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Product objects.
     * @throws PropelException
     */
    public function countProducts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductsPartial && !$this->isNew();
        if (null === $this->collProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProducts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProducts());
            }

            $query = ChildProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByShipment($this)
                ->count($con);
        }

        return count($this->collProducts);
    }

    /**
     * Method called to associate a ChildProduct object to this object
     * through the ChildProduct foreign key attribute.
     *
     * @param  ChildProduct $l ChildProduct
     * @return $this|\data_models\Shipment The current object (for fluent API support)
     */
    public function addProduct(ChildProduct $l)
    {
        if ($this->collProducts === null) {
            $this->initProducts();
            $this->collProductsPartial = true;
        }

        if (!$this->collProducts->contains($l)) {
            $this->doAddProduct($l);

            if ($this->productsScheduledForDeletion and $this->productsScheduledForDeletion->contains($l)) {
                $this->productsScheduledForDeletion->remove($this->productsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProduct $product The ChildProduct object to add.
     */
    protected function doAddProduct(ChildProduct $product)
    {
        $this->collProducts[]= $product;
        $product->setShipment($this);
    }

    /**
     * @param  ChildProduct $product The ChildProduct object to remove.
     * @return $this|ChildShipment The current object (for fluent API support)
     */
    public function removeProduct(ChildProduct $product)
    {
        if ($this->getProducts()->contains($product)) {
            $pos = $this->collProducts->search($product);
            $this->collProducts->remove($pos);
            if (null === $this->productsScheduledForDeletion) {
                $this->productsScheduledForDeletion = clone $this->collProducts;
                $this->productsScheduledForDeletion->clear();
            }
            $this->productsScheduledForDeletion[]= clone $product;
            $product->setShipment(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBank) {
            $this->aBank->removeShipment($this);
        }
        $this->id = null;
        $this->supplierdateload = null;
        $this->boatdateload = null;
        $this->loadport = null;
        $this->destinationport = null;
        $this->weekno = null;
        $this->noofcontainer = null;
        $this->exporter = null;
        $this->importer = null;
        $this->note = null;
        $this->cashoption = null;
        $this->invoicemaxprice = null;
        $this->notes = null;
        $this->bankcontact = null;
        $this->insured = null;
        $this->extrabruto = null;
        $this->shippingcost = null;
        $this->currency = null;
        $this->priceper = null;
        $this->customname = null;
        $this->genadd = null;
        $this->shipmentagend = null;
        $this->bank_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collProducts) {
                foreach ($this->collProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collProducts = null;
        $this->aBank = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ShipmentTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
