<?php
    require_once '/common.php';
    
    use data_models\ClientQuery as ClientQuery;
    use data_models\BankQuery as BankQuery;
    use data_models\CompanyQuery as CompanyQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0){
        $companyId = isset($data->companyId) ? $data->companyId : 0;
        $company = CompanyQuery::create()->findPk($companyId);
        
        $clients = ClientQuery::create()->filterByCompanyId($companyId)->find();
        
        $clientData = array();
        foreach($clients as $client) {
          array_push($clientData, array(
              "id" => $client->getId(),
              "clientName" => $client->getClientName(),
              "email" => $client->getEmail(),
              "telNo" => $client->getTelNo()
            ));
        }

        $data = array(
                "id" => $company->getId(),
                "companyName" => $company->getCompanyName(),
                "email" => $company->getEmail(),
                "telNo" => $company->getTelNo(),
                "clients" => $clientData);
            
        sendSuccessResponse($data, "Client details retrieved successfully.");
    }
?>