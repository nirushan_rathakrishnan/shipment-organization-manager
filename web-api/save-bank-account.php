<?php
    require_once '/common.php';
    
    use data_models\Bank as Bank;
    use data_models\BankQuery as BankQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);
    if($data && $data !== null){
        $bankId = isset($data->id) ? $data->id : '';
        $clientId = isset($data->clientId) ? $data->clientId : '';
        $bankName = isset($data->bankName) ? $data->bankName : '';
        $branchCode = isset($data->branchCode) ? $data->branchCode : '';
        $accountNumber = isset($data->accountNumber) ? $data->accountNumber : '';
        $city = isset($data->city) ? $data->city : '';
        $swiftCode = isset($data->swiftCode) ? $data->swiftCode : '';
        $bankAddress = isset($data->bankAddress) ? $data->bankAddress : '';
        $telNo = isset($data->telNo) ? $data->telNo : '';
        $fax = isset($data->fax) ? $data->fax : '';
        $website = isset($data->website) ? $data->website : '';
        $email = isset($data->email) ? $data->email : '';
        $accountName = isset($data->accountName) ? $data->accountName : '';
        $currency = isset($data->currency) ? $data->currency : '';
    } else {
        $bankId = $_POST['id'];
        $clientId = $_POST['clientId'];
        $bankName = $_POST['bankName'];
        $branchCode = $_POST['branchCode'];
        $accountNumber = $_POST['accountNumber'];
        $city = $_POST['city'];
        $swiftCode = $_POST['swiftCode'];
        $bankAddress = $_POST['bankAddress'];
        $telNo = $_POST['telNo'];
        $fax = $_POST['fax'];
        $website = $_POST['website'];
        $email = $_POST['email'];
        $accountName = $_POST['accountName'];
        $currency = $_POST['currency'];
    }

    if($bankId && $bankId > 0) {
        $bankAccount = BankQuery::create()->findOneById($bankId);
    } else {
        $bankAccount = new Bank();
    }
    
    $bankAccount->setClientId($clientId);
    $bankAccount->setBankName($bankName);
    $bankAccount->setBranchCode($branchCode);
    $bankAccount->setAccountNumber($accountNumber);
    $bankAccount->setCity($city);
    $bankAccount->setSwiftCode($swiftCode);
    $bankAccount->setBankAddress($bankAddress);
    $bankAccount->setTelNo($telNo);
    $bankAccount->setFax($fax);
    $bankAccount->setWebsite($website);
    $bankAccount->setEmail($email);
    $bankAccount->setAccountName($accountName);
    $bankAccount->setCurrency($currency);

    $bankAccount->save();
    sendSuccessResponse($data, "Bank account is saved successfully.");
?>