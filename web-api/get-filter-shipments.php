<?php
    require_once '/common.php';
    
    use data_models\ShipmentQuery as ShipmentQuery;
    use data_models\ClientQuery as ClientQuery;
    use data_models\ProductQuery as ProductQuery;
    use data_models\BankQuery as BankQuery;
    
    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0){
        $filter = isset($data->filter) ? $data->filter : null;

        $shipments = ShipmentQuery::create()->filterByWeekNo($filter->value)->find();
        
        $data = array();
        foreach($shipments as $shipment) {
            $products = ProductQuery::create()->filterByShipmentId($shipment->getId())->find();
            $bank = BankQuery::create()->findPk($shipment->getBankId());
          array_push($data, array(
              "id" => $shipment->getId(),
              "client" => ClientQuery::create()->findPk($bank->getClientId())->getClientName(), 
              "destinationPort" => $shipment->getDestinationPort(), 
              "loadPort" => $shipment->getLoadPort(),
              "boatDateLoad" => $shipment->getBoatDateLoad(), 
              "supplierDateLoad" => $shipment->getSupplierDateLoad(),
              "exporter" => $shipment->getExporter(),
              "importer" => $shipment->getImporter(),
              "noOfContainer" => $shipment->getNoOfContainer(),
              "categoryNo" => count($products),
              "weekNo" => $shipment->getWeekNo()));
        }
    
        sendSuccessResponse($data, "Shipments details retrieved successfully.");
    }
?>