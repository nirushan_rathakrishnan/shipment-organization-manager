<?php
    require_once '/common.php';

    use data_models\ClientQuery as ClientQuery;
    use data_models\BankQuery as BankQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null) {
        $data = $_POST;
    }

    if($data && $data !== null){
        $clientId = isset($data->clientId) ? $data->clientId : 0;
    } else {
        $clientId = $_POST['clientId'];
    }

    $banks = BankQuery::create()->filterByClientId($clientId)->find();
    if(count($banks) > 0) {
        sendErrorResponse(["Cannot delete this client since some bank accounts are related to this client."]);
    } else {
        ClientQuery::create()->filterById($clientId)->delete();
        sendSuccessResponse($clientId, "Client is deleted successfully.");
    }
?>