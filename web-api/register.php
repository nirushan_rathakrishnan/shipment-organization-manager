<?php
	require_once '/common.php';

	use data_models\User as User;
	
	$request_body = file_get_contents('php://input');
	$data = json_decode($request_body);

	$email = trim(strtolower($data->email), " ");
	$password = $data->password;
	$confirmPassword = $data->confirmPassword;
	
	if($password !== $confirmPassword) {
		sendErrorResponse(array(0 => 'Password and confirm password are not matched.'));
	}
	
	$user = new User();
	$user->setEmail($email);
	$user->setPassword($password);

	if (!$user->validate()) {
		$errorMsgs = array();
		foreach ($user->getValidationFailures() as $failure) {
			array_push($errorMsgs, "Property ".$failure->getPropertyPath().": ".$failure->getMessage());
		}
		sendErrorResponse($errorMsgs);
	}
	else {
		$user->save();
		sendSuccessResponse($data, "User is registered successfully.");
	}
?>