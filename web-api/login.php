<?php
	require_once '/common.php';

    use data_models\User as User;
    use data_models\UserQuery as UserQuery;
    use \Firebase\JWT\JWT as JWT;
    
	$request_body = file_get_contents('php://input');
    $data = json_decode($request_body);
    if($data && $data !== null){
        $email = isset($data->email) ? $data->email : '';
        $password = isset($data->password) ? $data->password : '';
    } else {
        $email = $_POST['email'];
        $password = $_POST['password'];
    }

    $user = UserQuery::create()->findOneByEmail(strtolower(trim($email, " ")));
    
	if ($user !== null && $user->getPassword() === $password) {
        $tokenId    = base64_encode(mcrypt_create_iv(32));
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;             //Adding 10 seconds
        $expire     = $notBefore + 60;            // Adding 60 seconds
        $serverName = 'nirushan'; // Retrieve the server name from config file
        
        /*
         * Create the token as an array
         */
        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
            'data' => [                  // Data related to the signer user
                'userId'   => 1, // userid from the users table
                'userName' => 'nirushan', // User name
            ]
        ];

        $secretKey = base64_decode('scretkeymykey');

        $jwt = JWT::encode(
            $data,      //Data to be encoded in the JWT
            $secretKey, // The signing key
            'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
            );

        sendSuccessResponse($jwt, 'User is loggedin successfully.');
	}
	else {
		sendErrorResponse(array(0 => 'Username or password is incorrect.'));
	}
?>