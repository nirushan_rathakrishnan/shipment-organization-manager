<?php
    require_once '/common.php';

    use data_models\ShipmentQuery as ShipmentQuery;
    use data_models\ProductQuery as ProductQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null) {
        $data = $_POST;
    }

    if($data && $data !== null){
        $shipmentId = isset($data->shipmentId) ? $data->shipmentId : 0;
    } else {
        $shipmentId = $_POST['shipmentId'];
    }

    ProductQuery::create()->filterByShipmentId($shipmentId)->delete();
    ShipmentQuery::create()->filterById($shipmentId)->delete();

    sendSuccessResponse($shipmentId, "Shipment is deleted successfully.");
?>