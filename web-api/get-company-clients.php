<?php
    require_once '/common.php';
    
    use data_models\ClientQuery as ClientQuery;
    use data_models\CompanyQuery as CompanyQuery;
    
    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0){
        $companyId = isset($data->companyId) ? $data->companyId : 0;

        $clients = ClientQuery::create()->filterByCompanyId($companyId)->find();
        
        $data = array();
        foreach($clients as $client) {
            array_push($data, array(
                "id" => $client->getId(), 
                "clientName" => $client->getClientName(), 
                "telNo" => $client->getTelNo(), 
                "email" => $client->getEmail()));
        }

        sendSuccessResponse($data, "Clients details retrieved successfully.");
    }
?>