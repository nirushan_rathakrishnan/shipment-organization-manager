<?php
    require_once '/common.php';
    
    use data_models\BankQuery as BankQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0){
        $clientId = isset($data->clientId) ? $data->clientId : 0;

        $banks = BankQuery::create()->filterByClientId($clientId)->find();
        
        $data = array();
        foreach($banks as $bank) {
        array_push($data, array(
            "id" => $bank->getId(), 
            "bankName" => $bank->getBankName(), 
            "telNo" => $bank->getTelNo(), 
            "email" => $bank->getEmail()));
        }

        sendSuccessResponse($data, "Clients details retrieved successfully.");
    }
?>