<?php
    require_once '/common.php';
    
    use data_models\Company as Company;
    use data_models\CompanyQuery as CompanyQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0) {
        $id = isset($data->id) ? $data->id : 0;
        $companyName = isset($data->companyName) ? $data->companyName : '';
        $telNo = isset($data->telNo) ? $data->telNo : '';
        $email = isset($data->email) ? $data->email : '';

        if($id && $id > 0) {
            $company = CompanyQuery::create()->findOneById($id);
        } else {
            $company = new Company();
        }
        
        $company->setCompanyName($companyName);
        $company->setTelNo($telNo);
        $company->setEmail($email);
    
        $company->save();
        sendSuccessResponse($data, "Company is saved successfully.");
    }
?>