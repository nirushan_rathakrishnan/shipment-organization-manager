<?php
    require_once '/common.php';
    
    use data_models\ClientQuery as ClientQuery;
    use data_models\BankQuery as BankQuery;
    use data_models\CompanyQuery as CompanyQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0){
        $clientId = isset($data->clientId) ? $data->clientId : 0;
        $client = ClientQuery::create()->findPk($clientId);
        
        $banks = BankQuery::create()->filterByClientId($clientId)->find();
        
        $bankData = array();
        foreach($banks as $bank) {
          array_push($bankData, array(
              "id" => $bank->getId(),
              "bankName" => $bank->getBankName(),
              "email" => $bank->getEmail(),
              "accountNo" => $bank->getAccountNumber(),
              "branchCode" => $bank->getBranchCode(),
              "telNo" => $bank->getTelNo()
            ));
        }

        $data = array(
                "id" => $client->getId(),
                "clientName" => $client->getClientName(), 
                "companyId" => $client->getCompanyId(),
                "companyName" => CompanyQuery::create()->findPk($client->getCompanyId())->getCompanyName(),
                "email" => $client->getEmail(),
                "telNo" => $client->getTelNo(),
                "banks" => $bankData);
            
        sendSuccessResponse($data, "Client details retrieved successfully.");
    }
?>