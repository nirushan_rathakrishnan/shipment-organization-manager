
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- company
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `companyName` VARCHAR(255) NOT NULL,
    `telNo` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- client
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `clientName` VARCHAR(255) NOT NULL,
    `telNo` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `company_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `client_fi_2986fb` (`company_id`),
    CONSTRAINT `client_fk_2986fb`
        FOREIGN KEY (`company_id`)
        REFERENCES `company` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- bank
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `bankName` VARCHAR(255) NOT NULL,
    `branchCode` VARCHAR(255) NOT NULL,
    `accountNumber` VARCHAR(255) NOT NULL,
    `city` VARCHAR(255) NOT NULL,
    `swiftCode` VARCHAR(255) NOT NULL,
    `bankAddress` VARCHAR(255) NOT NULL,
    `telNo` VARCHAR(255) NOT NULL,
    `fax` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `website` VARCHAR(255) NOT NULL,
    `accountName` VARCHAR(255) NOT NULL,
    `currency` VARCHAR(255) NOT NULL,
    `client_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `bank_fi_90166c` (`client_id`),
    CONSTRAINT `bank_fk_90166c`
        FOREIGN KEY (`client_id`)
        REFERENCES `client` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- product
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `productName` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `buyPrice` VARCHAR(255) NOT NULL,
    `sellPrice` VARCHAR(255) NOT NULL,
    `shipment_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `product_fi_251767` (`shipment_id`),
    CONSTRAINT `product_fk_251767`
        FOREIGN KEY (`shipment_id`)
        REFERENCES `shipment` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- shipment
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `shipment`;

CREATE TABLE `shipment`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `supplierDateLoad` VARCHAR(255) NOT NULL,
    `boatDateLoad` VARCHAR(255) NOT NULL,
    `loadPort` VARCHAR(255) NOT NULL,
    `destinationPort` VARCHAR(255) NOT NULL,
    `weekNo` VARCHAR(255) NOT NULL,
    `noOfContainer` VARCHAR(255) NOT NULL,
    `exporter` VARCHAR(255) NOT NULL,
    `importer` VARCHAR(255) NOT NULL,
    `note` VARCHAR(255) NOT NULL,
    `cashOption` VARCHAR(255) NOT NULL,
    `invoiceMaxPrice` VARCHAR(255) NOT NULL,
    `notes` VARCHAR(255) NOT NULL,
    `bankContact` VARCHAR(255) NOT NULL,
    `insured` VARCHAR(255) NOT NULL,
    `extraBruto` VARCHAR(255) NOT NULL,
    `shippingCost` VARCHAR(255) NOT NULL,
    `currency` VARCHAR(255) NOT NULL,
    `pricePer` VARCHAR(255) NOT NULL,
    `customName` VARCHAR(255) NOT NULL,
    `genAdd` VARCHAR(255) NOT NULL,
    `shipmentAgend` VARCHAR(255) NOT NULL,
    `bank_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `shipment_fi_4fe2a0` (`bank_id`),
    CONSTRAINT `shipment_fk_4fe2a0`
        FOREIGN KEY (`bank_id`)
        REFERENCES `bank` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
