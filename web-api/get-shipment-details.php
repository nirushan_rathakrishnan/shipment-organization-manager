<?php
    require_once '/common.php';
    
    use data_models\ShipmentQuery as ShipmentQuery;
    use data_models\ClientQuery as ClientQuery;
    use data_models\BankQuery as BankQuery;
    use data_models\CompanyQuery as CompanyQuery;
    use data_models\ProductQuery as ProductQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0){
        $shipmentId = isset($data->shipmentId) ? $data->shipmentId : 0;
        $shipment = ShipmentQuery::create()->findPk($shipmentId);
        
        $products = ProductQuery::create()->filterByShipmentId($shipmentId)->find();
        
        $productData = array();
        foreach($products as $product) {
          array_push($productData, array(
              "id" => $product->getId(),
              "productName" => $product->getProductName(), 
              "description" => $product->getDescription(),
              "sellPrice" => $product->getSellPrice(), 
              "buyPrice" => $product->getBuyPrice()));
        }

        $bank = BankQuery::create()->findPk($shipment->getBankId());
        $client = ClientQuery::create()->findPk($bank->getClientId());
        $data = array(
                "id" => $shipment->getId(),
                "companyId" => $client->getCompanyId(),
                "clientId" => $client->getId(),
                "clientId" => $client->getId(),
                "bankId" => $shipment->getBankId(),
                "companyName" => CompanyQuery::create()->findPk($client->getCompanyId())->getCompanyName(),
                "clientName" => $client->getClientName(), 
                "bankName" => $bank->getBankName(),
                "destinationPort" => $shipment->getDestinationPort(), 
                "loadPort" => $shipment->getLoadPort(),
                "boatDateLoad" => $shipment->getBoatDateLoad(), 
                "supplierDateLoad" => $shipment->getSupplierDateLoad(),
                "weekNo" => $shipment->getWeekNo(),
                "noOfContainer" => $shipment->getNoOfContainer(),
                "exporter" => $shipment->getExporter(),
                "importer" => $shipment->getImporter(),
                "note" => $shipment->getNote(),
                "cashOption" => $shipment->getCashOption(),
                "invoiceMaxPrice" => $shipment->getInvoiceMaxPrice(),
                "notes" => $shipment->getNotes(),
                "insured" => $shipment->getInsured(),
                "extraBruto" => $shipment->getExtraBruto(),
                "shippingCost" => $shipment->getShippingCost(),
                "currency" => $shipment->getCurrency(),
                "pricePer" => $shipment->getPricePer(),
                "customName" => $shipment->getCustomName(),
                "genAdd" => $shipment->getGenAdd(),
                "shipmentAgend" => $shipment->getShipmentAgend(),
                "products" => $productData);
            
        sendSuccessResponse($data, "Shipment details retrieved successfully.");
    }
?>