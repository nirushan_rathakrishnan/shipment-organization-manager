<?php
    require_once '/common.php';

    use data_models\ClientQuery as ClientQuery;
    use data_models\CompanyQuery as CompanyQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null) {
        $data = $_POST;
    }

    if($data && $data !== null){
        $companyId = isset($data->companyId) ? $data->companyId : 0;
    } else {
        $companyId = $_POST['companyId'];
    }

    $clients = ClientQuery::create()->filterByCompanyId($companyId)->find();
    if(count($clients) > 0) {
        sendErrorResponse(["Cannot delete this company since some clients are related to this company."]);
    } else {
        CompanyQuery::create()->filterById($companyId)->delete();
        sendSuccessResponse($companyId, "Company is deleted successfully.");
    }
?>