<?php
    require_once '/common.php';

    use data_models\ClientQuery as ClientQuery;
    use data_models\BankQuery as BankQuery;
    use data_models\ShipmentQuery as ShipmentQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null) {
        $data = $_POST;
    }

    if($data && $data !== null){
        $bankId = isset($data->bankId) ? $data->bankId : 0;
    } else {
        $bankId = $_POST['bankId'];
    }

    $shipments = ShipmentQuery::create()->filterByBankId($bankId)->find();
    if(count($shipments) > 0) {
        sendErrorResponse(["Cannot delete this bank since some shipments are related to this bank."]);
    } else {
        BankQuery::create()->filterById($bankId)->delete();
        sendSuccessResponse($bankId, "Bank is deleted successfully.");
    }
?>