<?php
	header('Access-Control-Allow-Headers: Content-Type');
	// setup the autoloading
	require_once '/vendor/autoload.php';
	// setup Propel
	require_once '/generated-conf/config.php';
	
	$result = array();
	
	function sendErrorResponse($errorMsgs) {
		global $result;
		$result['isError'] = true;
		$result['errorMessages'] = $errorMsgs;
		//http_response_code(400);
		echo json_encode($result);
		exit;
	}
	
	function sendSuccessResponse($data, $successMessage = 'Operation executed successfully.') {
		global $result;
		$result['isError'] = false;
		$result['successMessage'] = $successMessage;
		$result['data'] = $data;
		echo json_encode($result);
		exit;
	}
?>