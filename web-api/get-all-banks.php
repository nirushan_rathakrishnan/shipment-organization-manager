<?php
    require_once '/common.php';
    
    use data_models\ClientQuery as ClientQuery;
    use data_models\BankQuery as BankQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null) {
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0) {
        $companyId = isset($data->companyId) ? $data->companyId : 0;
        $clientId = isset($data->clientId) ? $data->clientId : 0;
        if($companyId == 0) {
            $banks = BankQuery::create()->find();
        } else if($companyId > 0 && $clientId == 0) {
            $clients = ClientQuery::create()->filterByCompanyId($companyId)->find();
            $banks = array();
            foreach($clients as $client) {
                $clientBanks = BankQuery::create()->filterByClientId($client->getId())->find();
                foreach($clientBanks as $bank) {
                    array_push($banks, $bank);
                }
            }
        } else if($clientId > 0) {
            $banks = BankQuery::create()->filterByClientId($clientId)->find();
        }
    }
    
    
    $data = array();
    foreach($banks as $bank) {
      array_push($data, array(
          "id" => $bank->getId(), 
          "clientName" => ClientQuery::create()->findPk($bank->getClientId())->getClientName(), 
          "bankName" => $bank->getBankName(), 
          "telNo" => $bank->getTelNo(), 
          "accountNo" => $bank->getAccountNumber(),
          "branchCode" => $bank->getBranchCode(),
          "email" => $bank->getEmail()));
    }

    sendSuccessResponse($data, "Banks details retrieved successfully.");
?>