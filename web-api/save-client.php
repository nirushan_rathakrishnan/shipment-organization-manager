<?php
    require_once '/common.php';
    
    use data_models\Client as Client;
    use data_models\ClientQuery as ClientQuery;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);

    if(!$data || $data === null){
        $data = $_POST;
    }

    if($data && $data !== null && count($data) > 0) {
        $clientId = isset($data->id) ? $data->id : 0;
        $companyId = isset($data->companyId) ? $data->companyId : '';
        $clientName = isset($data->clientName) ? $data->clientName : '';
        $telNo = isset($data->telNo) ? $data->telNo : '';
        $email = isset($data->email) ? $data->email : '';
    
        if($clientId && $clientId > 0) {
            $client = ClientQuery::create()->findOneById($clientId);
        } else {
            $client = new Client();
        }

        $client->setCompanyId($companyId);
        $client->setClientName($clientName);
        $client->setTelNo($telNo);
        $client->setEmail($email);
    
        $client->save();
        sendSuccessResponse($data, "Client is saved successfully.");
    }
?>