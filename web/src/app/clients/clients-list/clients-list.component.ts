import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { DataTableModel, ColType } from '../../shared/modules/data-table/data-table.model';
import { ClientModel } from '../../home/system.model';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {
  clients = [];
  filterModel = {};
  dataTableModel: DataTableModel<ClientModel> = new DataTableModel<ClientModel>();
  displayClientPopup: boolean = false;
  selectedClient: ClientModel = new ClientModel();

  constructor(private apiService: ApiService, private router: Router, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.loadAllClients();
  }

  loadAllClients() {
    this.apiService.post("get-all-clients.php", {}).subscribe(
      result => {
        if (!result.isError) {
          this.clients = result.data;
          this.configureGridData();
        }
      }
    );
  }

  configureGridData() {
    this.dataTableModel.columns = [
      { field: 'clientName', header: 'Client Name', isSortable: true },
      { field: 'companyName', header: 'Company Name', isSortable: true },
      { field: 'email', header: 'Email', isSortable: true },
      { field: 'telNo', header: 'Tel No', isSortable: true },
      { field: 'action', header: 'Action', colType: ColType.Button }
    ];
    this.dataTableModel.commonActions = [
      {
        name: 'View',
        onClick: (client) => this.onViewDetailsClicked(client.id)
      },
      {
        name: 'Edit',
        onClick: (client) => this.onEditClicked(client.id)
      },
      {
        name: 'Delete',
        onClick: (client) => this.onDeleteClient(client.id)
      },
    ];
    this.dataTableModel.totalRecords = this.clients.length;
    this.dataTableModel.dataList = this.clients;
  }

  onViewDetailsClicked(clientId) {
    this.apiService.post("get-client-details.php", { clientId: clientId }).subscribe(
      result => {
        if (!result.isError) {
          this.selectedClient = result.data;
          this.displayClientPopup = true;
        }
      }
    );
  }

  onEditClicked(clientId) {
    this.router.navigate(['client/details/' + clientId]);
  }

  onDeleteClient(clientId) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this client?',
      accept: async () => {
        await this.deleteClient(clientId);
        this.displayClientPopup = false;
      }
    });
  }

  async deleteClient(clientId: number) {
    var result = await this.apiService.post("delete-client.php", { clientId: clientId }).toPromise();
    if (!result.isError) {
      this.loadAllClients();
    }
  }

}
