import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsComponent } from './clients.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { RouterModule } from '@angular/router';
import { ClientsRoutingModule } from './clients-routing.module';
import { FormsModule } from '@angular/forms';
import { NavBarModule } from '../shared/modules/nav-bar/nav-bar.module';
import { AdvancedDataTableModule } from '../shared/modules/data-table/data-table.module';
import { PopupModule } from '../shared/modules/popup/popup.module';

@NgModule({
  imports: [
    CommonModule,
    ClientsRoutingModule,
    RouterModule,
    FormsModule,
    NavBarModule,
    AdvancedDataTableModule,
    PopupModule
  ],
  declarations: [ClientsComponent, ClientsListComponent, ClientDetailsComponent]
})
export class ClientsModule { }
