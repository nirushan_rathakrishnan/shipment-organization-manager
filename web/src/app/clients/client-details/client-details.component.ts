import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientModel } from '../../home/system.model';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
  model: ClientModel = new ClientModel();
  companyList = [];
  isCompanyAvailable = false;

  constructor(private apiService: ApiService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    let clientId = this.route.snapshot.paramMap.get('id');
    if (clientId && clientId !== null) {
      var result = await this.apiService.post("get-client-details.php", { clientId: clientId }, false).toPromise();
      if (!result.isError) {
        this.model = result.data;
      }
    }

    this.apiService.post("get-all-companies.php", {}, false).subscribe(
      result => {
        if (!result.isError) {
          this.companyList = result.data;
          if (this.companyList.length > 0) {
            this.isCompanyAvailable = true;

            if (this.model.companyId === 0 || this.model.companyId === undefined)
              this.model.companyId = this.companyList[0].id;
          }
        }
      }
    );
  }

  saveBankAccount(form) {
    this.apiService.post('save-client.php', this.model).subscribe(
      result => {
        if (result.isError === false) {
          form.reset();
          this.model.companyId = this.companyList[0].id;
          this.router.navigate(['client/list']);
        }
      }
    );
  }

}
