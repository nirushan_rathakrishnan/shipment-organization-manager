import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { ClientsListComponent } from "./clients-list/clients-list.component";
import { ClientDetailsComponent } from "./client-details/client-details.component";
import { ClientsComponent } from "./clients.component";

const clientRoutes: Routes = [
    {
        path: 'client',
        component: ClientsComponent,
        children: [
            {
                path: 'list',
                component: ClientsListComponent
            },
            {
                path: 'details',
                component: ClientDetailsComponent
            },
            {
                path: 'details/:id',
                component: ClientDetailsComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(clientRoutes)
    ]
})
export class ClientsRoutingModule { }