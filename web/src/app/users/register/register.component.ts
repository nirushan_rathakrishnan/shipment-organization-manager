import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { RegisterData } from '../users.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: RegisterData = new RegisterData();

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {

  }

  registerUser() {
    this.apiService.post('register.php', this.model).subscribe(
      data => {
        this.router.navigate(['users/login']);
      }
    );
  }
}
