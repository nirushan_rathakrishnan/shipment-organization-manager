import { Component, OnInit } from '@angular/core';
import { LoginData } from '../users.model';
import { ApiService } from '../../shared/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: LoginData = new LoginData();

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('token') && localStorage.getItem('token') !== '') {
      this.router.navigate(['home']);
    }
  }

  loginUser() {
    this.apiService.post('login.php', this.model).subscribe(
      result => {
        if (result.isError === false) {
          localStorage.setItem('token', result.data);
          this.router.navigate(['home']);
        }
      }
    );
  }
}
