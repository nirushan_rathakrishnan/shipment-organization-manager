export class RegisterData {
    email: string;
    password: string;
    confirmPassword: string;
}

export class LoginData {
    email: string;
    password: string;
}