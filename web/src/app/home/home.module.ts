import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { RouterModule } from '@angular/router';
import { BankAccountComponent } from './bank-account/bank-account.component';
import { FormsModule } from '@angular/forms';
import { BankAccountListComponent } from './bank-account-list/bank-account-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PopupModule } from '../shared/modules/popup/popup.module';
import { NavBarModule } from '../shared/modules/nav-bar/nav-bar.module';
import { AdvancedDataTableModule } from '../shared/modules/data-table/data-table.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    RouterModule,
    FormsModule,
    BrowserAnimationsModule,
    PopupModule,
    NavBarModule,
    AdvancedDataTableModule
  ],
  declarations: [HomeComponent, BankAccountComponent, BankAccountListComponent]
})
export class HomeModule { }
