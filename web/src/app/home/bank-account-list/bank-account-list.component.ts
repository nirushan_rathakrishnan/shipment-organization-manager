import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { FilterModel, BankAccountModel } from '../system.model';
import { DataTableModel, ColType } from '../../shared/modules/data-table/data-table.model';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';

@Component({
  selector: 'app-bank-account-list',
  templateUrl: './bank-account-list.component.html',
  styleUrls: ['./bank-account-list.component.css']
})
export class BankAccountListComponent implements OnInit {
  bankAccounts = [];
  filterModel: FilterModel = { filterBy: "0", value: "" };
  filterCompanyId: number = 0;
  dataTableModel: DataTableModel<BankAccountModel> = new DataTableModel<BankAccountModel>();
  selectedBank: BankAccountModel = new BankAccountModel();
  displayBankPopup: boolean = false;
  clientList = [];
  companyList = [];

  constructor(private apiService: ApiService,
    private router: Router,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.loadAllBanks();
    this.loadAllCompanies();
  }

  loadAllBanks() {
    this.apiService.post("get-all-banks.php", { companyId: this.filterCompanyId, clientId: this.filterModel.filterBy }).subscribe(
      result => {
        if (!result.isError) {
          this.bankAccounts = result.data;
          this.configureGridData();
        }
      }
    );
  }

  configureGridData() {
    this.dataTableModel.columns = [
      { field: 'bankName', header: 'Bank Name', isSortable: true },
      { field: 'clientName', header: 'Client Name', isSortable: true },
      { field: 'accountNo', header: 'Account No', isSortable: true },
      // { field: 'branchCode', header: 'Branch Code', isSortable: true },
      { field: 'email', header: 'Email', isSortable: true },
      { field: 'telNo', header: 'Tel No', isSortable: true },
      { field: 'action', header: 'Action', colType: ColType.Button }
    ];
    this.dataTableModel.commonActions = [
      {
        name: 'View',
        onClick: (bank) => this.onViewDetailsClicked(bank.id)
      },
      {
        name: 'Edit',
        onClick: (bank) => this.onEditClicked(bank.id)
      },
      {
        name: 'Delete',
        onClick: (bank) => this.onDeleteBank(bank.id)
      },
    ];
    this.dataTableModel.totalRecords = this.bankAccounts.length;
    this.dataTableModel.dataList = this.bankAccounts;
  }

  onViewDetailsClicked(bankId: number) {
    this.apiService.post("get-bank-details.php", { bankId: bankId }).subscribe(
      result => {
        if (!result.isError) {
          this.selectedBank = result.data;
          this.displayBankPopup = true;
        }
      }
    );
  }

  onEditClicked(bankId: number) {
    this.router.navigate(['home/bank-accounts/' + bankId]);
  }

  onDeleteBank(bankId: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this shipment?',
      accept: async () => {
        await this.deleteBank(bankId);
        this.displayBankPopup = false;
      }
    });
  }

  async deleteBank(bankId) {
    var result = await this.apiService.post("delete-bank.php", { bankId: bankId }).toPromise();
    if (!result.isError) {
      this.loadAllBanks();
    }
  }

  onCompanyChanged() {
    this.filterModel.filterBy = "0";
    if (this.filterCompanyId === 0) {
      this.clientList = [];
    } else {
      this.loadCompanyClients();
    }
  }

  async loadAllCompanies() {
    let result = await this.apiService.post("get-all-companies.php", {}, false).toPromise();

    if (!result.isError) {
      this.companyList = result.data;
    }
  }

  async loadCompanyClients() {
    var result = await this.apiService.post("get-company-clients.php", { companyId: this.filterCompanyId }, false).toPromise();

    if (!result.isError) {
      this.clientList = result.data;
    }
  }

  onFilter() {
    this.loadAllBanks();
  }

  onRefresh() {
    this.clientList = [];
    this.filterModel.filterBy = "0";
    this.filterCompanyId = 0;
    this.loadAllBanks();
  }
}
