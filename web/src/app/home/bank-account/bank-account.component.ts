import { Component, OnInit } from '@angular/core';
import { BankAccountModel, ClientModel } from '../system.model';
import { ApiService } from '../../shared/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bank-account',
  templateUrl: './bank-account.component.html',
  styleUrls: ['./bank-account.component.css']
})
export class BankAccountComponent implements OnInit {
  model: BankAccountModel = new BankAccountModel();
  clientList = [];
  companyList = [];
  isClientAvailable = false;
  isCompanyAvailable = false;
  displayClientPopup = false;
  selectedClient: ClientModel = new ClientModel();
  
  constructor(private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute) { }

  async ngOnInit() {
    let bankId = this.route.snapshot.paramMap.get('id');
    if (bankId && bankId !== null) {
      var result = await this.apiService.post("get-bank-details.php", { bankId: bankId }, false).toPromise();
      if (!result.isError) {
        this.model = result.data;
      }
    }

    await this.loadAllCompanies();
    await this.loadCompanyClients();
  }

  async loadAllCompanies(refreshCompany: boolean = false) {
    let result = await this.apiService.post("get-all-companies.php", {}, false).toPromise();

    if (!result.isError) {
      this.companyList = result.data;

      if (this.model.companyId === undefined || this.model.companyId === 0 || refreshCompany) {
        this.loadFirstCompanyDetails();
      }

      if (this.companyList.length > 0) {
        this.isCompanyAvailable = true;
      }
    }
  }

  loadFirstCompanyDetails() {
    if (this.companyList.length > 0) {
      this.model.companyId = this.companyList[0].id;
    }
  }

  async loadCompanyClients(refreshClient: boolean = false) {
    this.isClientAvailable = false;

    var result = await this.apiService.post("get-company-clients.php", { companyId: this.model.companyId }, false).toPromise();
    if (!result.isError) {
      this.clientList = result.data;

      if (this.model.clientId === undefined || this.model.clientId === 0 || refreshClient) {
        this.loadFirstClientDetails();
      }

      if (this.clientList.length > 0) {
        this.isClientAvailable = true;
      }
    }
  }

  loadFirstClientDetails() {
    if (this.clientList.length > 0) {
      this.model.clientId = this.clientList[0].id;
    }
  }

  async onCompanyChanged() {
    await this.loadCompanyClients(true);
  }

  saveBankAccount(form) {
    this.apiService.post('save-bank-account.php', this.model).subscribe(
      result => {
        if (result.isError === false) {
          form.reset();
          this.loadFirstCompanyDetails();
          this.router.navigate(['home/bank-accounts-list']);
        }
      }
    );
  }

  onBankDetailsViewClick() {
    this.apiService.post("get-client-details.php", { clientId: this.model.clientId }).subscribe(
      result => {
        if (!result.isError) {
          this.selectedClient = result.data;
          this.displayClientPopup = true;
        }
      }
    );
  }
}
