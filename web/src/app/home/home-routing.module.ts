import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home.component";
import { BankAccountComponent } from "./bank-account/bank-account.component";
import { BankAccountListComponent } from "./bank-account-list/bank-account-list.component";

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        children: [
            {
                path: 'bank-accounts-list',
                component: BankAccountListComponent
            },
            {
                path: 'bank-accounts',
                component: BankAccountComponent
            },
            {
                path: 'bank-accounts/:id',
                component: BankAccountComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ]
})
export class HomeRoutingModule { }