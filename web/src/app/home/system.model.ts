export class BankAccountModel {
    id: number;
    companyId: number;
    clientId: number;
    bankName: string;
    branchCode: string;
    accountNumber: string;
    city: string;
    swiftCode: string;
    bankAddress: string;
    telNo: string;
    fax: string;
    website: string;
    email: string;
    accountName: string;
    clientName: string;
    currency: string;
    companyName: string;
}

export class ShipmentModel {
    supplierDateLoad: string;
    boatDateLoad: string;
    loadPort: string;
    destinationPort: string;
    weekNo: string;
    noOfContainer: string;
    containerCategoryNo: string;
    exporter: string;
    importer: string;
    clientId: number;
    companyId: number;
    note: string;
    cashOption: string;
    invoiceMaxPrice: string;
    notes: string;
    bankId: number;
    insured: string;
    extraBruto: string;
    shippingCost: string;
    products: Product[];
    id: number;
    bankName: string;
    companyName: string;
    clientName: string;
    currency: string;
    pricePer: string;
    customName: string;
    genAdd: string;
    shipmentAgend: string;
}

export class Product {
    productName: string;
    buyPrice: number;
    sellPrice: number;
    description: string;
}

export class ClientModel {
    clientName: string;
    email: string;
    telNo: string;
    companyId: number;
    id: number;
    banks: any;
    companyName: string;
}

export class FilterModel { 
    filterBy: string; 
    value: string;
}

export class CompanyModel { 
    id: number;
    companyName: string;
    email: string;
    telNo: string;
    clients: any;
}