import { Component, OnInit } from '@angular/core';
import { ShipmentModel, FilterModel } from '../../home/system.model';
import { ApiService } from '../../shared/api.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';
import { DataGridService } from '../../shared/modules/data-grid/data-grid.service';
import { DataTableModel, ColType } from '../../shared/modules/data-table/data-table.model';

@Component({
  selector: 'app-shipment-list',
  templateUrl: './shipment-list.component.html',
  styleUrls: ['./shipment-list.component.css']
})
export class ShipmentListComponent implements OnInit {
  shipments: ShipmentModel[];
  displayShipmentPopup = false;
  selectedShipment: ShipmentModel = new ShipmentModel();
  filterModel: FilterModel = { filterBy: "weekNo", value: "" };
  dataTableModel: DataTableModel<ShipmentModel> = new DataTableModel<ShipmentModel>();

  constructor(private apiService: ApiService,
    private router: Router,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.loadAllShipments();
  }

  onViewDetailsClicked(shipmentId: number) {
    this.apiService.post("get-shipment-details.php", { shipmentId: shipmentId }).subscribe(
      result => {
        if (!result.isError) {
          this.selectedShipment = result.data;
          this.displayShipmentPopup = true;
        }
      }
    );
  }

  onEditClicked(shipmentId: number) {
    this.router.navigate(['shipments/details/' + shipmentId]);
  }

  onDeleteShipment(shipmentId: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this shipment?',
      accept: async () => {
        await this.deleteShipment(shipmentId);
        this.displayShipmentPopup = false;
      }
    });
  }

  async deleteShipment(shipmentId: number) {
    await this.apiService.post("delete-shipment.php", { shipmentId: shipmentId }).toPromise();
    this.loadAllShipments();
  }

  loadAllShipments() {
    this.apiService.post("get-all-shipments.php", {}).subscribe(
      result => {
        if (!result.isError) {
          this.shipments = result.data;
          this.configureGridData();
        }
      }
    );
  }

  configureGridData() {
    this.dataTableModel.columns = [
      { field: 'exporter', header: 'Exporter', isSortable: true },
      { field: 'importer', header: 'Importer', isSortable: true },
      { field: 'client', header: 'Client', isSortable: true },
      { field: 'noOfContainer', header: 'No.Of.Cont', isSortable: true },
      { field: 'categoryNo', header: 'Category No', isSortable: true },
      { field: 'supplierDateLoad', header: 'Supplier Date Load', isSortable: true },
      { field: 'boatDateLoad', header: 'Boat Date Load', isSortable: true },
      { field: 'weekNo', header: 'Week No', isSortable: true },
      { field: 'action', header: 'Action', colType: ColType.Button }
    ];
    this.dataTableModel.commonActions = [
      {
        name: 'View',
        onClick: (shipment) => this.onViewDetailsClicked(shipment.id)
      },
      {
        name: 'Edit',
        onClick: (shipment) => this.onEditClicked(shipment.id)
      },
      {
        name: 'Delete',
        onClick: (shipment) => this.onDeleteShipment(shipment.id)
      },
    ];
    this.dataTableModel.totalRecords = this.shipments.length;
    this.dataTableModel.dataList = this.shipments;
  }

  onSearch() {
    this.apiService.post("get-filter-shipments.php", { filter: this.filterModel }).subscribe(
      result => {
        if (!result.isError) {
          this.shipments = result.data;
          this.configureGridData();
        }
      }
    );
  }

  onRefresh() {
    this.loadAllShipments();
    this.filterModel = { filterBy: "weekNo", value: "" };
  }
}
