import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { ShipmentComponent } from "./shipment.component";
import { ShipmentDetailsComponent } from "./shipment-details/shipment-details.component";
import { ShipmentListComponent } from "./shipment-list/shipment-list.component";

const routes: Routes = [
    {
        path: 'shipments',
        component: ShipmentComponent,
        children: [
            {
                path: 'details',
                component: ShipmentDetailsComponent
            },
            {
                path: 'details/:id',
                component: ShipmentDetailsComponent
            },
            {
                path: 'list',
                component: ShipmentListComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ]
})
export class ShipmentRoutingModule { }