import { Component, OnInit } from '@angular/core';
import { ShipmentModel, Product } from '../../home/system.model';
import { ApiService } from '../../shared/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-shipment-details',
  templateUrl: './shipment-details.component.html',
  styleUrls: ['./shipment-details.component.css']
})
export class ShipmentDetailsComponent implements OnInit {
  model: ShipmentModel = new ShipmentModel();
  clientList = [];
  companyList = [];
  isClientAvailable = false;
  isCompanyAvailable = false;
  popupProductModel: Product = new Product();

  selectedClientBankList = [];
  isBankAvailable = false;

  displayProductPopup: boolean = false;

  constructor(private apiService: ApiService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    let shipmentId = this.route.snapshot.paramMap.get('id');
    if (shipmentId && shipmentId !== null) {
      var result = await this.apiService.post("get-shipment-details.php", { shipmentId: shipmentId }, false).toPromise();
      if (!result.isError) {
        this.model = result.data;
      }
    } else {
      this.model.insured = 'Yes';
      this.model.cashOption = 'BankCash';
      this.model.currency = 'Dollars';
      this.model.pricePer = 'Kilo';
      this.model.customName = 'FAUD';
      this.model.genAdd = 'Yes';
      this.model.shipmentAgend = 'TEU';
    }
    await this.loadCompanies();
    await this.loadCompanyClients();
    await this.loadClientBanks();
  }

  async loadCompanies(refreshCompany: boolean = false) {
    this.isCompanyAvailable = false;

    var result = await this.apiService.post("get-all-companies.php", {}, false).toPromise();
    if (!result.isError) {
      this.companyList = result.data;

      if (this.model.companyId === undefined || this.model.companyId === 0 || refreshCompany) {
        this.loadFirstCompanyDetails();
      }

      if (this.companyList.length > 0) {
        this.isCompanyAvailable = true;
      }
    }
  }

  loadFirstCompanyDetails() {
    if (this.companyList.length > 0) {
      this.model.companyId = this.companyList[0].id;
    }
  }

  async loadCompanyClients(refreshClient: boolean = false) {
    this.isClientAvailable = false;

    var result = await this.apiService.post("get-company-clients.php", { companyId: this.model.companyId }, false).toPromise();
    if (!result.isError) {
      this.clientList = result.data;

      if (this.model.clientId === undefined || this.model.clientId === 0 || refreshClient) {
        this.loadFirstClientDetails();
      }

      if (this.companyList.length > 0) {
        this.isClientAvailable = true;
      }
    }
  }

  loadFirstClientDetails() {
    if (this.clientList.length > 0) {
      this.model.clientId = this.clientList[0].id;
    }
  }

  async loadClientBanks(refreshBank: boolean = false) {
    this.isBankAvailable = false;
    var result = await this.apiService.post("get-client-banks.php", { clientId: this.model.clientId }, false).toPromise();
    if (!result.isError) {
      this.selectedClientBankList = result.data;
      
      if (this.model.bankId === undefined || this.model.bankId === 0 || refreshBank) {
        this.loadFirstBankDetails();
      }

      if (this.selectedClientBankList.length > 0) {
        this.isBankAvailable = true;
      }
    }
  }

  loadFirstBankDetails() {
    if (this.selectedClientBankList.length > 0) {
      this.model.bankId = this.selectedClientBankList[0].id;
    }
  }

  async onCompanyChanged() {
    await this.loadCompanyClients(true);
    await this.loadClientBanks(true);
  }

  showContainerPopup() {
    this.displayProductPopup = true;
  }

  async onClientChanged() {
    await this.loadClientBanks(true);
  }

  saveShippingDetails(form) {
    this.apiService.post('save-shipment-details.php', this.model).subscribe(
      result => {
        if (result.isError === false) {
          form.reset();
          this.router.navigate(['shipments/list']);
        }
      }
    );
  }

  addProduct() {
    if (!this.model.products) {
      this.model.products = [];
    }
    this.model.products.push(this.popupProductModel);
    this.popupProductModel = new Product();
    this.displayProductPopup = false;
  }

  removeProduct(container: Product) {
    let index = this.model.products.indexOf(container);
    this.model.products.splice(index, 1);
  }
}
