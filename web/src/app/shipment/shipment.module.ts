import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShipmentDetailsComponent } from './shipment-details/shipment-details.component';
import { ShipmentComponent } from './shipment.component';
import { ShipmentRoutingModule } from './shipment-routing.module';
import { RouterModule } from '@angular/router';
import { PopupModule } from '../shared/modules/popup/popup.module';
import { FormsModule } from '@angular/forms';
import { NavBarModule } from '../shared/modules/nav-bar/nav-bar.module';
import { ShipmentListComponent } from './shipment-list/shipment-list.component';
import { AdvancedDataTableModule } from '../shared/modules/data-table/data-table.module';

@NgModule({
  imports: [
    CommonModule,
    ShipmentRoutingModule,
    RouterModule,
    PopupModule,
    FormsModule,
    NavBarModule,
    AdvancedDataTableModule
  ],
  declarations: [ShipmentDetailsComponent, ShipmentComponent, ShipmentListComponent]
})
export class ShipmentModule { }
