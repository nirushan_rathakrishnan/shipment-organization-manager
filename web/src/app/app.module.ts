import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { UsersModule } from './users/users.module';
import { MessageHandlerService } from './shared/message-handler.service';
import { ApiService } from './shared/api.service';
import { HomeModule } from './home/home.module';
import { ClientsModule } from './clients/clients.module';
import { CompaniesModule } from './companies/companies.module';
import { ShipmentModule } from './shipment/shipment.module';
import { ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';
import { DataGridModule } from './shared/modules/data-grid/data-grid.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ClientsModule,
    RouterModule,
    UsersModule,
    HomeModule,
    CompaniesModule,
    ShipmentModule,
    AppRoutingModule,
    ConfirmDialogModule
  ],
  providers: [MessageHandlerService, ApiService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
