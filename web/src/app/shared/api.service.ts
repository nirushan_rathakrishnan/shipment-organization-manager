import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { MessageHandlerService } from "./message-handler.service";
import 'rxjs/add/operator/share';

@Injectable()
export class ApiService {
    private static apiUrl = "http://localhost/shipment-organization-manager/web-api";

    constructor(private http: HttpClient, private errorMessageHandlerService: MessageHandlerService) { }

    post(action: string, data, isHandleMsgEnable = true) {
        let apiCallObservable = this.http.post(ApiService.apiUrl + '/' + action, data)
            .map(response => {
                return isHandleMsgEnable ? this.handleResponse(response) : response;
            })
            .share();

        this.handleCommonErrorResponse(apiCallObservable);

        return apiCallObservable;
    }

    handleResponse(response) {
        if (response.isError === true) {
            this.errorMessageHandlerService.announceErrorMessage(response.errorMessages.join(', '));
        } else {
            this.errorMessageHandlerService.announceSuccessMessage(response.successMessage);
        }
        return response;
    }

    handleCommonErrorResponse(apiCallObservable) {
        apiCallObservable.subscribe(
            data => {

            },
            err => {
                this.errorMessageHandlerService.announceErrorMessage(err._body);
            }
        );
    }
}