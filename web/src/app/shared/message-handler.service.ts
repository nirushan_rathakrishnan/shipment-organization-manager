import { Injectable } from "@angular/core";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessageHandlerService {
    private errorMessageAnnouncedSource = new Subject<string>();
    errorMessageAnnounced$ = this.errorMessageAnnouncedSource.asObservable();

    private successMessageAnnouncedSource = new Subject<string>();
    successMessageAnnounced$ = this.successMessageAnnouncedSource.asObservable();

    announceErrorMessage(errorMessage: string) {
        this.errorMessageAnnouncedSource.next(errorMessage);
        //clear message after 3 sec
        setTimeout(() => 
        { 
            this.errorMessageAnnouncedSource.next("");
        }, 6000);
    }

    announceSuccessMessage(successMessage: string) {
        this.successMessageAnnouncedSource.next(successMessage);
        //clear message after 3 sec
        setTimeout(() => 
        { 
            this.successMessageAnnouncedSource.next("");
        }, 6000);
    }
}