import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { DataTable } from "./data-grid.model";

@Injectable()
export class DataGridService<Model> {
    private dataGridSource = new Subject<DataTable<Model>>();
    dataGridDataAnnounced$ = this.dataGridSource.asObservable();

    addData(data: DataTable<Model>) {
        this.dataGridSource.next(data);
    }
}