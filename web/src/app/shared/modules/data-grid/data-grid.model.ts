export class DataTable<Model> {
    tableName?: string;
    headers: Header[];
    data?: Data<Model>[];
    commonActions?: Action<Model>[];
}

export class Header {
    name: string;
    prop: string;
}

export class Data<Model> {
    model: Model;
    dataValue: DataValue[];
    actions?: Action<Model>[];
}

export class DataValue {
    value: string;
    prop: string;
}

export class Action<Model> {
    name: string;
    onClick = (e: Model) => {};
}