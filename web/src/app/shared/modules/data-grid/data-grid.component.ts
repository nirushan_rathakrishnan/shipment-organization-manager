import { Component, OnInit, Input } from '@angular/core';
import { DataTable } from './data-grid.model';
import { DataGridService } from './data-grid.service';

@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.css']
})
export class DataGridComponent implements OnInit {
  @Input() 
  gridName: string;

  dataTable: DataTable<any> = {headers: []};

  constructor(private dataGridService: DataGridService<any>) { 
    dataGridService.dataGridDataAnnounced$.subscribe(
      data => {
        debugger;
        this.dataTable = data;
      }
    );
    dataGridService.addData({headers: []});
  }

  ngOnInit() {
    
  }

}
