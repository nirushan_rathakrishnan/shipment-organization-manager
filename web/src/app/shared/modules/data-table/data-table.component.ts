import { Component, OnInit, Input } from '@angular/core';
import { DataTableModel, SortEventData } from './data-table.model';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {
  @Input()
  dataTableModel: DataTableModel<any> = new DataTableModel();

  constructor() { }

  ngOnInit() {
  }

  changeSort(data: SortEventData) {
    this.dataTableModel.onChangeSort(data);
  }
}
