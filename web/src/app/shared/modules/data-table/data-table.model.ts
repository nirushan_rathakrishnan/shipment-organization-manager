export class DataTableModel<Model> {
    dataList: Model[];
    columns: Column[];
    onChangeSort?: (data: SortEventData) => any;
    commonActions?: Action<Model>[];
    totalRecords: number;
}

export class Column {
    field: string;
    header: string;
    isSortable?: boolean = false;
    colType?: ColType = ColType.Text;
}

export enum ColType {
    Text = 1,
    Button = 2
}

export class SortEventData {
    field: string;
}

export class Action<Model> {
    name: string;
    onClick = (e: Model) => { };
}