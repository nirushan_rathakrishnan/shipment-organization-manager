import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table.component';
import { DataTableModule, SharedModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule
  ],
  declarations: [DataTableComponent],
  exports: [DataTableComponent]
})
export class AdvancedDataTableModule { }
