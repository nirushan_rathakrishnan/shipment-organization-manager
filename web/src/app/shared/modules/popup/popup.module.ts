import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupComponent } from './popup.component';
import { DialogModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DialogModule
  ],
  declarations: [PopupComponent],
  exports: [PopupComponent]
})
export class PopupModule { }
