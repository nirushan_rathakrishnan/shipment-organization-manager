import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  _display: boolean = false;
  @Output()
  displayChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input()
  set display(value: boolean) {
    this._display = value;
    this.displayChange.emit(this._display);
  }
  get display() {
    return this._display;
  }
  @Input()
  title: string = "Popup";

  showDialog() {
    this.display = true;
  }

  constructor() { }

  ngOnInit() {
  }

}
