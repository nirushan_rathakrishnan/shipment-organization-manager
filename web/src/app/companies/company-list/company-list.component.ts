import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { CompanyModel } from '../../home/system.model';
import { DataTableModel, ColType } from '../../shared/modules/data-table/data-table.model';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  companies = [];
  displayCompanyPopup = false;
  selectedCompany: CompanyModel = new CompanyModel();
  dataTableModel: DataTableModel<CompanyModel> = new DataTableModel<CompanyModel>();

  constructor(private apiService: ApiService, 
    private router: Router, 
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.loadAllCompanies();
  }

  loadAllCompanies() {
    this.apiService.post("get-all-companies.php", {}).subscribe(
      result => {
        if (!result.isError) {
          this.companies = result.data;
          this.configureGridData();
        }
      }
    );
  }

  configureGridData() {
    this.dataTableModel.columns = [
      { field: 'companyName', header: 'Company Name', isSortable: true },
      { field: 'email', header: 'Email', isSortable: true },
      { field: 'telNo', header: 'Tel No', isSortable: true },
      { field: 'action', header: 'Action', colType: ColType.Button }
    ];
    this.dataTableModel.commonActions = [
      {
        name: 'View',
        onClick: (company) => this.onViewDetailsClicked(company.id)
      },
      {
        name: 'Edit',
        onClick: (company) => this.onEditClicked(company.id)
      },
      {
        name: 'Delete',
        onClick: (company) => this.onDeleteCompany(company.id)
      },
    ];
    this.dataTableModel.totalRecords = this.companies.length;
    this.dataTableModel.dataList = this.companies;
  }

  onViewDetailsClicked(companyId) {
    this.apiService.post("get-company-details.php", { companyId: companyId }).subscribe(
      result => {
        if (!result.isError) {
          this.selectedCompany = result.data;
          this.displayCompanyPopup = true;
        }
      }
    );
  }

  onEditClicked(companyId) {
    this.router.navigate(['company/details/' + companyId]);
  }

  onDeleteCompany(companyId) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this company?',
      accept: async () => {
        await this.deleteCompany(companyId);
        this.displayCompanyPopup = false;
      }
    });
  }

  async deleteCompany(companyId: number) {
    var result = await this.apiService.post("delete-company.php", { companyId: companyId }).toPromise();
    if (!result.isError) {
      this.loadAllCompanies();
    }
  }
}
