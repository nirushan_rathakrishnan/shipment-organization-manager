import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompaniesComponent } from './companies.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { CompaniesRoutingModule } from './companies-routing.module';
import { RouterModule } from '@angular/router';
import { NavBarModule } from '../shared/modules/nav-bar/nav-bar.module';
import { FormsModule } from '@angular/forms';
import { AdvancedDataTableModule } from '../shared/modules/data-table/data-table.module';
import { PopupModule } from '../shared/modules/popup/popup.module';

@NgModule({
  imports: [
    CommonModule,
    CompaniesRoutingModule,
    RouterModule,
    NavBarModule,
    FormsModule,
    AdvancedDataTableModule,
    PopupModule
  ],
  declarations: [CompaniesComponent, CompanyListComponent, CompanyDetailsComponent]
})
export class CompaniesModule { }
