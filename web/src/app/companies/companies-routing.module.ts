import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CompaniesComponent } from "./companies.component";
import { CompanyDetailsComponent } from "./company-details/company-details.component";
import { CompanyListComponent } from "./company-list/company-list.component";

const companiesRoutes: Routes = [
    {
        path: 'company',
        component: CompaniesComponent,
        children: [
            {
                path: 'list',
                component: CompanyListComponent
            },
            {
                path: 'details',
                component: CompanyDetailsComponent
            },
            {
                path: 'details/:id',
                component: CompanyDetailsComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(companiesRoutes)
    ]
})
export class CompaniesRoutingModule { }