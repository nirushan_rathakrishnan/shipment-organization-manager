import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css']
})
export class CompanyDetailsComponent implements OnInit {
  model: { companyName: string, email: string, telNo: string } = { companyName: '', email: '', telNo: '' };

  constructor(private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute) { }

  async ngOnInit() {
    let companyId = this.route.snapshot.paramMap.get('id');
    if (companyId && companyId !== null) {
      var result = await this.apiService.post("get-company-details.php", { companyId: companyId }, false).toPromise();
      if (!result.isError) {
        this.model = result.data;
      }
    }
  }

  saveBankAccount(form) {
    this.apiService.post('save-company.php', this.model).subscribe(
      result => {
        if (result.isError === false) {
          form.reset();
          this.router.navigate(['company/list']);
        }
      }
    );
  }
}
