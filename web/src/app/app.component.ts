import { Component } from '@angular/core';
import { MessageHandlerService } from './shared/message-handler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  errorMessage = '';
  successMessage = '';

  constructor(private messageHandlerService: MessageHandlerService) {
    messageHandlerService.errorMessageAnnounced$.subscribe(
      errorMessage => {
        this.successMessage = '';
        this.errorMessage = errorMessage;
      }
    );

    messageHandlerService.successMessageAnnounced$.subscribe(
      successMessage => {
        this.errorMessage = '';
        this.successMessage = successMessage;
      }
    );
  }
}
