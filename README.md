SETUP PROJECT
clone project
run
php composer.phar install


SETUP ENVIORNMENT
download wget (if you don't have) https://eternallybored.org/misc/wget/
set environment path for wget

wget http://getcomposer.org/composer.phar
# If you haven't wget on your computer
curl -s http://getcomposer.org/installer | php

php composer.phar install

set environment path for propel PROJECT_PATH/path/to/vendor/bin/

add Access-Control-Allow-Origin inside Directory in httpd.conf (path for wamp server : C:\wamp64\bin\apache\apache2.4.23\conf)
<Directory "${INSTALL_DIR}/www/">
	Header set Access-Control-Allow-Origin "*"
	...
</Directory>

change config as follows in httpd-vhosts.conf (C:\wamp64\bin\apache\apache2.4.23\conf\extra)
# Virtual Hosts
#

<VirtualHost *:80>
	ServerName localhost
	DocumentRoot c:/wamp64/www
	<Directory  "c:/wamp64/www/">
		Options +Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>
#

WEBSITE TESTING
------------
copy all files in website folder (organization-manager\website) and paste it inside (c:\wamp\www)

copy web-api folder in wamp/www/organization-manager

create a databse in phpMyAdmin called (shipment_system)
execute shipment_system.sql

access following url in the pc (for api testing)
http://localhost/organization-manager/web-api/test.php

access following url in the browser
http://localhost/index.html


ANDROID APP TESTING
--------------------
get wamp server running PC ip (ipconfig in command prompt)
access following url from android mobile (for connectivity test)
http://SERVER_IP/organization-manager/web-api/test.php

install apk file in your android mobile (app-release.apk)
enter email, password and server url and try to login

CONNECT ANDROID DEVICE VIA WIFI FOR DEBUGGING
---------------------------------------------
CONNECT DEVICE VIA USB CABLE.

$ adb usb
restarting in USB mode
$ adb devices
List of devices attached
$ adb tcpip 5555
restarting in TCP mode port: 5555
$ adb connect IP_ADDRESS_OF_THE_DEVICE
connected to #.#.#.#:5555
$ adb devices
List of devices attached
#.#.#.#:5555 device

REMOVE USB CABLE AND START TO DEBUG.

for restarting
$adb kill-server
"adb connect" step again.

GIT PUSH
---------------------------------------------
git add .
git commit -m "Initial commit"
git push -u origin master